﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CourseworkSystemB._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Friends of Paws subsystem B</h1>
        <p class="lead">Choose page above in the menu or click on the buttons.</p>
    </div>
    <div class="row">
        <table  style="width: 62%;" align="center">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" 
                        onclick="Button1_Click" Text="Adopt a pet" Width="181px" Height="43px" />
                </td>
              
            </tr>
            <tr>
                <td >
                    <asp:Button ID="Button2" runat="server" 
                        onclick="Button2_Click" Text="Hall of Fame" Width="181px" Height="43px" />
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Button ID="Button3" runat="server" 
                        onclick="Button3_Click" Text="Staff room" Width="181px" Height="43px" />
                </td>
            </tr>
        </table>
    </div>

    </asp:Content>
