﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourseworkSystemB
{
    public class User
    {
        int userId;
        string userName;
        string userEmail;
        string userAddress;
        string userMobile;
        string userCountry;
        string userImage;
       string petName;

        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string PetName
        {
            get { return petName; }
            set { petName = value; }
        }

        public string UserEmail
        {
            get { return userEmail; }
            set { userEmail = value; }
        }

        public string UserAddress
        {
            get { return userAddress; }
            set { userAddress = value; }
        }

        public string UserMobile
        {
            get { return userMobile; }
            set { userMobile = value; }
        }

        public string UserCountry
        {
            get { return userCountry; }
            set { userCountry = value; }
        }

        public string UserImage
        {
            get { return userImage; }
            set { userImage = value; }
        }

        
        public User(int id, string name, string email, string address, string mobile, string country, string image, string pet)
        {
            userId = id;
            userName = name;
            userEmail = email;
            userAddress = address;
            userMobile = mobile;
            userCountry = country;
            userImage = image;
            petName = pet;
        }
    }
}