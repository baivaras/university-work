﻿<%@ Page Title="Adopt" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adopt.aspx.cs" Inherits="CourseworkSystemB.Adopt" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Adopt a pet</h1>
        <p>
            
        </p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <asp:Button ID="Button2" runat="server" Text="List pets" OnClick="Button2_Click" />
            <br />
            <asp:Table ID="PetsTable" runat="server" Width="273px"
                 GridLines="Horizontal" >
            <asp:TableRow runat="server">
                <asp:TableCell runat="server"  ForeColor="Black">ID</asp:TableCell>
                <asp:TableCell runat="server"  ForeColor="Black">Pet</asp:TableCell>
                <asp:TableCell runat="server"  ForeColor="Black">Breed</asp:TableCell>                
                <asp:TableCell runat="server"  ForeColor="Black">Gender</asp:TableCell>
            </asp:TableRow>

            </asp:Table>
            <asp:Label ID="PetLabel" runat="server" Text="Choose a pet"></asp:Label>
            <br />
            <asp:DropDownList ID="PetDrop" runat="server">
            </asp:DropDownList>
            <br />
        </div>
        <%--<div class="col-md-4">
            <asp:Label ID="BreedLabel" runat="server" Text="Choose a breed"></asp:Label>
            <br />
            <asp:DropDownList ID="BreedDropDownList" runat="server"></asp:DropDownList>
            <br />
        </div>--%>
         <div class="col-md-4">

             <asp:Label ID="DonationLabel" runat="server" Text="Donation ammount"></asp:Label>
             &nbsp;(Suggested minimum amount is: Cats £50.00 / Dogs £100 / Ferrets £20.00 / Chinchillas £20.00 / Guinea Pigs £15.00 / Rabbits £30.00, bigger donation will appear on a hall of fame with your name! )<br />
             <asp:TextBox ID="DonationTextBox" runat="server"></asp:TextBox>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <br />
            <asp:Label ID="NameLabel" runat="server" Text="Name"></asp:Label>
            <br />
            <asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="EmailLabel" runat="server" Text="Email "></asp:Label>
            <br />
            <asp:TextBox ID="EmailTextBox" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="AddressLabel" runat="server" Text="Address"></asp:Label>
            <br />
            <asp:TextBox ID="AddressTextBox" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="MobileLabel" runat="server" Text="Mobile number"></asp:Label>
            <br />
            <asp:TextBox ID="MobileTextBox" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="CountryLabel" runat="server" Text="Country"></asp:Label>
            <br />
            <asp:TextBox ID="CountryTextBox" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="CalendarLabel" runat="server" Text="Choose a adoption date"></asp:Label>
            <br />
            <asp:Calendar ID="Calendar" runat="server"></asp:Calendar>
        </div>
        <div class="col-md-4">
            <asp:Image ID="UImage" runat="server" Height="140px" Width="140px" />
            <br />
            <br />
            <asp:FileUpload ID="FileUpload1" runat="server" />
        </div>
    </div>
    <div class="row" style="text-align: center">
        <asp:Button ID="Button1" runat="server" Text="Adopt a pet" onclick="Button1_Click" Height="72px" Width="223px" />
        <br />
        <asp:Label ID="LabelOutput" runat="server"></asp:Label>
    </div>
</asp:Content>
