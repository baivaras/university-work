﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourseworkSystemB
{
    public class Fame
    {

        int fameId;
        string fameName;
        string fameCountry;
        float fameDonation;
        string famePetName;

        public int FameId
        {
            get { return fameId; }
            set { fameId = value; }
        }

        public string FameName
        {
            get { return fameName; }
            set { fameName = value; }
        }

        public string FameCountry
        {
            get { return fameCountry; }
            set { fameCountry = value; }
        }

        public float FameDonation
        {
            get { return fameDonation; }
            set { fameDonation = value; }
        }

        public string FamePetName
        {
            get { return famePetName; }
            set { famePetName = value; }
        }

        public Fame (int id, string name, string country, float donation, string pet)
        {
            fameId = id;
            fameName = name;
            fameCountry = country;
            fameDonation = donation;
            FamePetName = pet;
        }

    }
}