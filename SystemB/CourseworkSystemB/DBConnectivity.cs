﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace CourseworkSystemB
{
    public class DBConnectivity
    {
        private static SqlConnection GetConnection()
        {
            string connString;
            connString = @"Data Source=SQL-SERVER;Initial Catalog=ab0264b;Persist Security Info=True;User ID=ab0264b;Password=Smerting@s1";
            return new SqlConnection(connString);
        }


        public static string TotalAmount()
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "SELECT SUM(userDonation) FROM User2";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                string sum = myCommand.ExecuteScalar().ToString();
                return sum;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }
        //method that saves a user in the db
        public static void SaveUser(string uN, string uE, string uA, string uM, string uCountry, float uDon, DateTime uDate, string uImg, string uPet)
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "INSERT INTO User2(userName, userEmail, userAddress, userMobile, userCountry, userDate, userImage, userDonation, petName) VALUES ('" + uN + "' , '" + uE + "' , '" + uA + "' , '" + uM + "' , '" + uCountry + "' , '" + uDate + "' , '" + uImg + "' , '" + uDon + "' , '" + uPet + "')";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }

        //method that saves a user to hall of Fame database
        public static void SaveUserFame(string uN, string uCountry, float uDon, string uPet)
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "INSERT INTO UserFame(fameName, fameCountry, fameDonation, famePetName) VALUES ('" + uN + "' , '" + uCountry + "' , '" + uDon + "', '" + uPet + "')";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }

        // method loads all the pets from database
        public static List<Pets> LoadAllPets()
        {
            List<Pets> pets = new List<Pets>();
            SqlConnection myConnection = GetConnection();
            string myQuery = "SELECT petId, petName, petGender, breedId FROM Pets";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            List<Breeds> breeds = LoadBreeds();
            //List<User> users = LoadUsers();

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    Breeds currentBreed = FindBreed(breeds, int.Parse(myReader["breedId"].ToString()));
                    Pets v = new Pets(int.Parse(myReader["petId"].ToString()), myReader["petName"].ToString(), myReader["petGender"].ToString(), currentBreed);
                    pets.Add(v);
                }
                return pets;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static List<User> LoadAllUsers()
        {
            List<User> users = new List<User>();
            SqlConnection myConnection = GetConnection();
            string myQuery = "SELECT * FROM User2";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    User g = new User(int.Parse(myReader["userId"].ToString()), myReader["userName"].ToString(), myReader["userEmail"].ToString(), myReader["userAddress"].ToString(), myReader["userMobile"].ToString(), myReader["userCountry"].ToString(), myReader["userImage"].ToString(), myReader["petName"].ToString());
                    users.Add(g);
                }
                return users;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }


        // method to load all Fame users from database
        public static List<Fame> LoadAllFame()
        {
            List<Fame> fames = new List<Fame>();
            SqlConnection myConnection = GetConnection();
            string myQuery = "SELECT * FROM UserFame";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    Fame g = new Fame(int.Parse(myReader["fameId"].ToString()), myReader["fameName"].ToString(), myReader["fameCountry"].ToString(), float.Parse(myReader["fameDonation"].ToString()), myReader["famePetName"].ToString());
                    fames.Add(g);
                }
                return fames;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        // Method that returns a list of Breeds objects with the details from the DB
        public static List<Breeds> LoadBreeds()
        {
            List<Breeds> breeds = new List<Breeds>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT * FROM Breeds";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Breeds g = new Breeds(int.Parse(myReader["breedId"].ToString()), myReader["breedName"].ToString());
                    breeds.Add(g);
                }
                return breeds;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        // Method to remove user from db

        public static void RemoveUser(int uId)
        {
            SqlConnection myConnection = GetConnection();

            string myQuery = "REMOVE FROM User2 WHERE userId = " + uId;

            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }


        public static List<User> LoadAdoptionbyPet(int uId)
        {
            List<User> users = new List<User>();
            SqlConnection myConnection = GetConnection();
            string myQuery = "SELECT userId, userName, userCountry, petName FROM User2 WHERE userId = " + uId;
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    User v = new User(int.Parse(myReader["userId"].ToString()), myReader["userName"].ToString(), myReader["userEmail"].ToString(), myReader["userAddress"].ToString(), myReader["userMobile"].ToString(), myReader["userCountry"].ToString(), myReader["userImage"].ToString(), myReader["petName"].ToString());
                    users.Add(v);
                }
                return users;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }


        private static User FindUser(List<User> users, int id)
        {
            foreach (var user in users)
            {
                if (user.UserId == id)
                {
                    return user;
                }
            }
            return null;
        }

        private static Breeds FindBreed(List<Breeds> breeds, int id)
        {
            foreach (var breed in breeds)
            {
                if (breed.BreedId == id)
                {
                    return breed;
                }
            }
            return null;
        }

        private static Pets FindPets(List<Pets> pets, int id)
        {
            foreach (var pet in pets)
            {
                if (pet.PetId == id)
                {
                    return pet;
                }
            }
            return null;
        }
    }
}