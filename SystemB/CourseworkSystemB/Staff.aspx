﻿<%@ Page Title="Staff Room" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Staff.aspx.cs" Inherits="CourseworkSystemB.Staff" %>

    <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

        <div class="row">
            <div class="col-md-4">

                <asp:Label ID="PetListLabel" runat="server" Text="Show adoptions by pet"></asp:Label>
                <br />
                <asp:DropDownList ID="PetDropDown" runat="server" Width="133px">
                </asp:DropDownList>

                <br />
                <asp:Button ID="SearchByPet" runat="server" Text="Search By Pet" OnClick="SearchByPet_Click" />
                <br />

                <br />

            </div>
            <div class="col-md-4">

                <asp:Label ID="BreedListLabel" runat="server" Text="Show adoptions by breed"></asp:Label>
                <br />
                <asp:ListBox ID="BreedListBox" runat="server" Height="19px" Width="154px"></asp:ListBox>

                <br />
                <asp:Button ID="SearchByBreed" runat="server" Text="Search By Breed" OnClick="SearchByBreed_Click" />
                <br />

                <br />

            </div>
            <div class="col-md-4">

                <asp:Label ID="UserNameListLabel" runat="server" Text="Show adoptions by adopter name"></asp:Label>
                <br />
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>

                <br />
                <asp:Button ID="SearchByName" runat="server" Text="Search By Customer Name" OnClick="SearchByName_Click" />
                <br />

                <br />

            </div>
        </div>
        <div class="row">
            <div class="col-md-9">

                <asp:Label ID="AdoptionTableLabel" runat="server" Text="Adoptions"></asp:Label>
                <br />
                <asp:Table ID="AdoptionTable" runat="server" Width="273px"
                 GridLines="Horizontal" Height="79px" >
            <asp:TableRow runat="server">
                <asp:TableCell runat="server"  ForeColor="Black">ID</asp:TableCell>
                <asp:TableCell runat="server"  ForeColor="Black">Pet</asp:TableCell>
                <asp:TableCell runat="server"  ForeColor="Black">Breed</asp:TableCell>                
                <asp:TableCell runat="server"  ForeColor="Black">Customer name</asp:TableCell>
            </asp:TableRow>

            </asp:Table>

                <br />

            </div>
             <div class="col-md-3">

                 <asp:Label ID="AmountDonatenLabel" runat="server" Text="Total amount donated: " style="font-weight: 700; text-decoration: underline"></asp:Label>

                 <asp:Label ID="AmountDonated" runat="server" Text=""></asp:Label>
                 <br />

            </div>
        </div>
        <div class="row">

            <br />
            <asp:Label ID="RemoveUserLabel" runat="server" Text="Remove user from database"></asp:Label>
            <br />

            <asp:DropDownList ID="RemoveUser" runat="server" Width="140px">
            </asp:DropDownList>
            <asp:Button ID="RemoveUser_Button" runat="server" Text="Remove User" OnClick="RemoveUser_Button_Click" />

        </div>
   </asp:Content>
