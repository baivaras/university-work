﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourseworkSystemB
{
    public class Pets
    {
        int petId;
        string petGender;
        string petName;
        Breeds bId;

        public int PetId
        {
            get { return petId; }
            set { petId = value; }
        }

        public string PetGender
        {
            get { return petGender; }
            set { petGender = value; }
        }

        public string PetName
        {
            get { return petName; }
            set { petName = value; }
        }

        public Breeds BId
        {
            get { return bId; }
            set { bId = value; }
        }

      

        public Pets(int id, string name, string gender, Breeds b)
        {
            petId = id;
            petName = name;
            petGender = gender;
            bId = b;
        }
    }
}