﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CourseworkSystemB
{
    public partial class Halloffame : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<Fame> fames = DBConnectivity.LoadAllFame();

                foreach (var fame in fames)
                {
                    TableRow row = new TableRow();
                    TableCell cell1 = new TableCell();
                    cell1.Text = fame.FameId.ToString();
                    TableCell cell2 = new TableCell();
                    cell2.Text = fame.FameName;
                    TableCell cell3 = new TableCell();
                    cell3.Text = fame.FameCountry;
                    TableCell cell4 = new TableCell();
                    cell4.Text = fame.FamePetName;
                    TableCell cell5 = new TableCell();
                    cell5.Text = fame.FameDonation.ToString();
                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    row.Cells.Add(cell5);
                    FameTable.Rows.Add(row);
                }
            }
        }
    }
}