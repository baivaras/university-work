﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CourseworkSystemB
{
    public partial class Adopt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PetDrop.DataSource = DBConnectivity.LoadAllPets();
                PetDrop.DataTextField = "PetName";
                PetDrop.DataValueField = "PetName"; //the name of the property that returns the ID
                PetDrop.DataBind();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
            List<Pets> pets = DBConnectivity.LoadAllPets();

            foreach (var pet in pets)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Text = pet.PetId.ToString();
                TableCell cell2 = new TableCell();
                cell2.Text = pet.PetName;
                TableCell cell3 = new TableCell();
                cell3.Text = pet.BId.BreedName;
                TableCell cell4 = new TableCell();
                cell4.Text = pet.PetGender;
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                PetsTable.Rows.Add(row);
            }
        }

        
        public float totalAmountDonated;
        protected void Button1_Click(object sender, EventArgs e)
        {
    
            string a = NameTextBox.Text;
            string b = EmailTextBox.Text;
            string c = AddressTextBox.Text;
            string d = MobileTextBox.Text;
            string f = CountryTextBox.Text;
            DateTime g = Calendar.SelectedDate;
            string s = PetDrop.SelectedValue.ToString();


            if (NameTextBox.Text != "" && EmailTextBox.Text != "" && AddressTextBox.Text != "" && MobileTextBox.Text != "" && CountryTextBox.Text != "" && DonationTextBox.Text != "" && FileUpload1.HasFile)
            {

                string str = FileUpload1.PostedFile.FileName;
                FileUpload1.PostedFile.SaveAs(Server.MapPath("~/upload/" + str));
                float h = float.Parse(DonationTextBox.Text);


                if (PetDrop.SelectedValue.Contains("Cat") && h > 50 || PetDrop.SelectedValue.Contains("Dog") && h > 100 || PetDrop.SelectedValue.Contains("Ferret") && h > 20 || PetDrop.SelectedValue.Contains("Chinchilla") && h > 20 || PetDrop.SelectedValue.Contains("Guinea Pig") && h > 15 || PetDrop.SelectedValue.Contains("Rabbit") && h > 30)
                    {
                    DBConnectivity.SaveUserFame(a,f,h,s);
                    LabelOutput.Text = "Congratulations " + a + " you have adopted a pet! And because your large donation you will appear on Hall of Fame!";

                }

            
                DBConnectivity.SaveUser(a,b,c,d,f,h,g,str,s);

                
                LabelOutput.Text = "Congratulations " + a + " you have adopted a pet!";
                EmailTextBox.Text = "";
                AddressTextBox.Text = "";
                MobileTextBox.Text = "";
                CountryTextBox.Text = "";
                str = "";
                NameTextBox.Text = "";
            }
            else
            {
                LabelOutput.Text = "You have to fill all possible fields, upload your image and choose a date!";
            }

        }
    }
}