﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CourseworkSystemB
{
    public partial class Staff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PetDropDown.DataSource = DBConnectivity.LoadAllUsers();
                PetDropDown.DataTextField = "UserId";
                PetDropDown.DataValueField = "UserId";
                PetDropDown.DataBind();

                RemoveUser.DataSource = DBConnectivity.LoadAllUsers();
                RemoveUser.DataTextField = "UserName";
                RemoveUser.DataValueField = "UserId";
                RemoveUser.DataBind();

                AmountDonated.Text = DBConnectivity.TotalAmount();
            }
        }

        protected void SearchByPet_Click(object sender, EventArgs e)
        {
            List<User> users = DBConnectivity.LoadAdoptionbyPet(int.Parse(PetDropDown.SelectedValue));


            foreach (var user in users)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Text = user.UserId.ToString();
                TableCell cell2 = new TableCell();
                cell2.Text = user.PetName;
                TableCell cell3 = new TableCell();
                cell3.Text = user.UserCountry;
                TableCell cell4 = new TableCell();
                cell4.Text = user.UserName;
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                AdoptionTable.Rows.Add(row);
            }
        }

        protected void SearchByBreed_Click(object sender, EventArgs e)
        {

        }

        protected void SearchByName_Click(object sender, EventArgs e)
        {

        }

        protected void RemoveUser_Button_Click(object sender, EventArgs e)
        {
            int user = int.Parse(RemoveUser.SelectedValue);
            DBConnectivity.RemoveUser(user);

            RemoveUserLabel.Text = "User removed";

        }
    }
}