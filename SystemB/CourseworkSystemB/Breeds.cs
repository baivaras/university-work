﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourseworkSystemB
{
    public class Breeds
    {
        int breedsId;
        string breedsName;

        public int BreedId
        {
            get { return breedsId; }
            set { breedsId = value; }
        }

        public string BreedName
        {
            get { return breedsName; }
            set { breedsName = value; }
        }

        public Breeds(int id, string name)
        {
            breedsId = id;
            breedsName = name;
        }
    }
}