<?php include('functions.php') ?>
<?php include('header.php') ?>
                        <h5 class="card-title text-center">Log In</h5>
                        <form class="form-signin" method="post" action="login.php">
                            <?php include('errors.php'); ?>
                            <?php 
                                        if(isset($_SESSION['msg'])) { ?>
                            <div class="alert alert-danger">
                                    <span>
                                      <?php 
                                        if(isset($_SESSION['msg'])) {
                                            echo $_SESSION['msg']; 
                                            unset($_SESSION['msg']);
                                        }
                                       
                                      ?>
                                    </span>
                                </div> <?php } ?>
                            <div class="form-label-group">
                                <?php  if(isset($_COOKIE['user'])) { ?>
                                     <input class="form-control" id="username" type="text" name="username" placeholder="Username" value="<?php echo $_COOKIE['user']; ?>">
                                <?php } else { ?>
                                    <input class="form-control" id="username" type="text" name="username" placeholder="Username">
                                <?php } ?>
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="password" name="password" placeholder="Password">
                            </div>
                            <div class="checkbox" >
                                <label><input type="checkbox" name="remember" id="remember" value="remember"> Remember me</label>
                              </div>
                            <div class="input-group">
                                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" name="login_user">Login</button>
                            </div>
                            <p>
                                Not yet a member? <a href="register.php">Sign up</a>
                            </p>
                        </form>
<script>


</script>

<?php include('footer.php') ?>