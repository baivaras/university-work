<?php include('functions.php') ?>
<?php include('header.php') ?>
                        <h5 class="card-title text-center">Sign In</h5>
                        <form class="form-signin" method="post" action="register.php">
                            <?php include('errors.php'); ?>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="username" placeholder="Username" value="<?php echo $username; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="email" name="email" placeholder="example@example.com" value="<?php echo $email; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="skill" placeholder="Plumber, Mechanic etc." value="<?php echo $skill; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="password" name="password_1" placeholder="Password">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="password" name="password_2" placeholder="Confirm Password">
                            </div>
                            <div class="form-label-group">
                                <img src="captcha.php" width="120" height="30" border="1" alt="CAPTCHA">
                                <p><input class="form-control" type="text" size="6" maxlength="5" name="captcha" value="" placeholder="Captcha">
                                    <small class="form-text text-muted">copy the digits from the image into this box</small></p>
                            </div>
                            <div class="form-label-group">
                                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" name="reg_user">Register</button>
                            </div>

                            <p>
                                Already a member? <a href="login.php">Sign in</a>
                            </p>
                        </form>
<?php include('footer.php') ?>