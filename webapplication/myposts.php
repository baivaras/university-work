<?php include('functions.php') ?>

<?php 
    require 'mysql.php';
    $link = mysqli_connect($host, $user, $passwd, $dbName);
    
    if (!isset($_SESSION['username'])) {
        $_SESSION['msg'] = "You must log in first to see you posts";
        header('location: login.php');
      }

    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header("location: login.php");
      }
    ?>
<?php include('header-fluid.php') ?>
                        
                        <h5 class="card-title text-center">Dashboard</h5>
<?php include('errors.php'); ?>
<?php 
                                        if(isset($_SESSION['msg'])) { ?>
                            <div class="alert alert-danger">
                                    <span>
                                      <?php 
                                        if(isset($_SESSION['msg'])) {
                                            echo $_SESSION['msg']; 
                                            unset($_SESSION['msg']);
                                        }
                                       
                                      ?>
                                    </span>
                                </div> <?php } ?>
<!--                        <form class="form-signin" method="post" action="dashboard.php">-->
                        <div class="row">
                            <div class="col-sm-4">
                                    <input class="form-control" id="keywords" type="text" placeholder="Search" onkeyup="searchFilterMyPosts()">
                                    
<!--
                            <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="imagescheckbox" name="imagescheckbox">
                                    <label class="form-check-label" for="exampleCheck1">Show posts with images</label>
                                </div>
-->
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" id="searchLocation" type="text" placeholder="Search by location" onkeyup="searchFilterMyPosts()">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-check">
                                    
                                    <select class="form-control" id="skillsSelect" onchange="searchFilterMyPosts()">
                                        <option value="" selected="selected" >Search by skill</option>
                                        <?php
                                            $sel_query="SELECT skillsReq FROM posts ORDER BY id asc";
                                            $result = mysqli_query($link,$sel_query);

                                            while($row = mysqli_fetch_array($result)) {
                                                echo '<option value="'.$row['skillsReq'].'">' .$row['skillsReq']. '</option>';  
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            </div>
</br>
                            <div id="posts_content" class="row">
                            
                                <?php
                                //Include pagination class file
                                include('pagination.php');
                                $username = $_SESSION['username'];
                                $limit = 5;

                                //get number of rows
                                $queryNum = $link->query("SELECT COUNT(*) as postNum FROM posts ");
                                $resultNum = $queryNum->fetch_assoc();
                                $rowCount = $resultNum['postNum'];
                                //echo $rowCount;

                                //initialize pagination class
                                $pagConfig = array(
                                    'totalRows' => $rowCount,
                                    'perPage' => $limit,
                                    'link_func' => 'searchFilterMyPosts'
                                );
                                $pagination =  new Pagination($pagConfig);

                                //get rows
                                $query = $link->query("SELECT * FROM posts WHERE username='$username' ORDER BY id DESC LIMIT $limit");

                                if($query->num_rows > 0){ ?>
                                <div class="table-responsive">
                        <table id="dashboardTable" class="display table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Post Title</th>
                                    <th scope="col">Post Image</th>
                                    <th scope="col">Post Text</th>
                                    <th scope="col">Service Type</th>
                                    <th scope="col">Skills Required</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Points award</th>
                                    <th scope="col">Service Status</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php 
                                while($row = mysqli_fetch_array($query)) {
                                    //$imageURL = 'uploads/'.$row["postImage"];
                                    $id = $row['id'];
                                    $imgquery = "SELECT id, imageName, altText FROM images WHERE idfk=$id LIMIT 1";
                                    $imgresult = mysqli_query($link,$imgquery);
                                    //echo $imgresult;
                                    echo "<tr>";
                                    echo '<th scope="row">' . $row['id'] . '</th>';
                                    echo '<td>' . $row['postTitle'] . '</td>';
                                    echo '<td>';
                                   // while($imgrow = mysqli_fetch_array($imgresult)) {
                                       for ( $i = 0 ; $i < mysqli_num_rows($imgresult) ; $i++ ) {
                                        $imgrow = mysqli_fetch_assoc($imgresult);
                                        echo '<img src="getImage.php?id=' . $imgrow['id'] . '" alt="' . $imgrow['altText'] . '" title="' . $imgrow['imageName']  .'" width="80" height="80"/>  ' . "\n";
                                      }
                                   // }
                                    echo '</td>';
                                    echo '<td>' . $row['postText'] . '</td>';
                                    echo '<td>' . $row['serviceType'] . '</td>';
                                    echo '<td>' . $row['skillsReq'] . '</td>';
                                    echo '<td>' . $row['location'] . '</td>';
                                    echo '<td>' . $row['award'] . '</td>';
                                    echo '<td>' . $row['serviceStatus'] . '</td>';
                                    echo '<td><a class="btn btn-outline-success waves-effect" href="edit.php?id=' . $row['id'] . '">Edit</a></td>';
                                    echo '<td><a class="btn btn-outline-danger waves-effect" href="delete.php?id=' . $row['id'] . '">Delete</a></td></td>';
                                    echo "</tr>";
                        }?>
                            </tbody>
                                    </table></div>
                        <?php }?>
                  <?php include('footer.php') ?>