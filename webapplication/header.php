<!DOCTYPE html>
<html>

<head>
    <title>Web application Coursework</title>

<!--    Bootstrap materials is borrowed from https://getbootstrap.com/ -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="script.js"></script>
    


    <script>
        $(function() {
            $('.poper').popover({
                container: 'body',
                trigger: 'focus'
            })
        })
    </script>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="mx-auto order-0">
                <a class="navbar-brand mx-auto" href="index.php">Web Application</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
            </div>
            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link btn btn-outline-success waves-effect" href="index.php">Home Page</a>
                    </li>
                    <?php if (isset($_SESSION['username'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link btn btn-outline-warning waves-effect" href="myposts.php">My Posts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-outline-info waves-effect" href="post.php">New Post</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-outline-primary waves-effect" href="?logout">Log out</a>
                    </li>
                    <span class="navbar-text text-center margintopbot">
                        <?php echo $_SESSION['username']; ?>
                    </span>
                    <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link btn btn-outline-primary waves-effect" href="login.php">Log In</a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container m50">
        <div class="row h100 align-items-center">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin">
                    <div class="card-body">