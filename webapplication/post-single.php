<?php include('functions.php'); ?>
<?php 
if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first to see posts";
    header('location: login.php');
}

if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
}

//getting id from url
$id = $_GET['id'];
 
//selecting data associated with this particular id
$query = "SELECT * FROM posts WHERE id=$id";
$result = mysqli_query($link, $query);
 
while($res = mysqli_fetch_array($result))
{
    $id = $res['id'];
    $imgquery = "SELECT id, imageName, altText FROM images WHERE idfk=$id";
    $imgresult = mysqli_query($link,$imgquery);
    $imgrow2 = mysqli_fetch_assoc($imgresult);
    $postTitle = $res['postTitle'];
    $postText = $res['postText'];
    $serviceType = $res['serviceType'];
    $skillsReq = $res['skillsReq'];
    $location = $res['location'];
    $award = $res['award']; 
}
?>

<?php include('header-fluid.php') ?>

<div class="card-title mb-4">
    <div class="d-flex justify-content-start">
        <div class="image-container">
             <?php echo '<img class="img-thumbnail" src="getImage.php?id=' . $imgrow2['id'] . '" alt="' . $imgrow2['altText'] . '" title="' . $imgrow2['imageName']  .'" width="150" height="150"/>  ';?>
            
        </div>
        <div class="userData ml-3">
            <h2 class="d-block"><?php echo $postTitle; ?></h2>
            <h6 class="d-block">Location: <?php echo $location; ?></h6>
            <h6 class="d-block">Award for the job: <?php echo $award; ?></h6>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <h3 class="d-block">Information</h3><hr/>
        <div class="tab-content ml-1" id="myTabContent">
            <div class="fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">


                <div class="row">
                    <div class="col-sm-3 col-md-2 col-5">
                        <label style="font-weight:bold;">Post Description</label>
                    </div>
                    <div class="col-md-8 col-6">
                        <?php echo $postText; ?>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-sm-3 col-md-2 col-5">
                        <label style="font-weight:bold;">Service</label>
                    </div>
                    <div class="col-md-8 col-6">
                        <?php echo $serviceType; ?>
                    </div>
                </div>
                <hr />


                <div class="row">
                    <div class="col-sm-3 col-md-2 col-5">
                        <label style="font-weight:bold;">Skills required</label>
                    </div>
                    <div class="col-md-8 col-6">
                        <?php echo $skillsReq; ?>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-sm-3 col-md-2 col-5">
                        <label style="font-weight:bold;">Service status</label>
                    </div>
                    <div class="col-md-8 col-6">
                        <?php echo $serviceStatus; ?>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-sm-3 col-md-2 col-5">
                        <label style="font-weight:bold;">Images</label>
                    </div>
                    <div class="col-md-8 col-6">
                        <div class="row">
                                <?php 
                                      for ( $i = 0 ; $i < mysqli_num_rows($imgresult) ; $i++ ) {
                                        $imgrow = mysqli_fetch_assoc($imgresult);
                                        echo '<div class="col-sm-3"><img class="img-thumbnail" src="getImage.php?id=' . $imgrow['id'] . '" alt="' . $imgrow['altText'] . '" title="' . $imgrow['imageName']  .'" /></div>  ';
                                      }
                                ?>
                            </div>
                    </div>
                </div>
                <hr />

            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>