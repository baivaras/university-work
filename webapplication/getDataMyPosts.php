<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include('functions.php');


if (isset($_POST['page'])) {
    //Include pagination class file
    
    include('pagination.php');
    
    
    require 'mysql.php';
    $link = mysqli_connect($host, $user, $passwd, $dbName);
    
    $start = !empty($_POST['page']) ? $_POST['page'] : 0;
    $limit = 5;
    
    //set conditions for search
    $username = $_SESSION['username'];

    $whereSQL     = $orderSQL = 'WHERE username='$username'';
    $keywords     = $_POST['keywords'];
    $location     = $_POST['searchLocation'];
    $skillsSelect = $_POST['skillsSelect'];
    //$sortBy = $_POST['sortBy'];
    if (!empty($keywords) && !empty($location) && !empty($skillsSelect)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')
                                         AND location LIKE '%" . $location . "%'
                                         AND skillsReq='$skillsSelect'
                                         AND username='$username'";
    } elseif (!empty($keywords) && !empty($location)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')
                                         AND location LIKE '%" . $location . "%'
                                         AND username='$username'";
    } elseif (!empty($location) && !empty($skillsSelect)) {
        $whereSQL = "WHERE location='$location' 
                                         AND skillsReq='$skillsSelect'
                                         AND username='$username'";
    } elseif (!empty($keywords) && !empty($skillsSelect)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%') 
                                         AND skillsReq='$skillsSelect'
                                         AND username='$username'";
    } elseif (!empty($keywords)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')
                                         AND username='$username'";
    } elseif (!empty($location)) {
        $whereSQL = "WHERE location LIKE '%" . $location . "%'
                                         AND username='$username'";
    } elseif (!empty($skillsSelect)) {
        $whereSQL = "WHERE skillsReq='$skillsSelect'
                                         AND username='$username'";
    }
    
    //    if(!empty($sortBy)){
    //        $orderSQL = " ORDER BY id ".$sortBy;
    //    }else{
    //        $orderSQL = " ORDER BY id DESC ";
    //    }
    
    //get number of rows
    $queryNum  = $link->query("SELECT COUNT(*) as postNum FROM posts $whereSQL");
    $resultNum = $queryNum->fetch_assoc();
    $rowCount  = $resultNum['postNum'];
    //initialize pagination class
    $pagConfig = array(
        'currentPage' => $start,
        'totalRows' => $rowCount,
        'perPage' => $limit,
        'link_func' => 'searchFilter'
    );
    
    $pagination = new Pagination($pagConfig);
    
    //get rows
    $query = $link->query("SELECT * FROM posts $whereSQL LIMIT $start,$limit");
    
    if ($query->num_rows > 0) {
        while($row = mysqli_fetch_array($query)) {
                                    //$imageURL = 'uploads/'.$row["postImage"];
                                    $id = $row['id'];
                                    $imgquery = "SELECT id, imageName, altText FROM images WHERE idfk=$id LIMIT 1";
                                    $imgresult = mysqli_query($link,$imgquery);
                                    //echo $imgresult;
                                    echo "<tr>";
                                    echo '<th scope="row">' . $row['id'] . '</th>';
                                    echo '<td>' . $row['postTitle'] . '</td>';
                                    echo '<td>';
                                   // while($imgrow = mysqli_fetch_array($imgresult)) {
                                       for ( $i = 0 ; $i < mysqli_num_rows($imgresult) ; $i++ ) {
                                        $imgrow = mysqli_fetch_assoc($imgresult);
                                        echo '<img src="getImage.php?id=' . $imgrow['id'] . '" alt="' . $imgrow['altText'] . '" title="' . $imgrow['imageName']  .'" width="80" height="80"/>  ' . "\n";
                                      }
                                   // }
                                    echo '</td>';
                                    echo '<td>' . $row['postText'] . '</td>';
                                    echo '<td>' . $row['serviceType'] . '</td>';
                                    echo '<td>' . $row['skillsReq'] . '</td>';
                                    echo '<td>' . $row['location'] . '</td>';
                                    echo '<td>' . $row['award'] . '</td>';
                                    echo '<td>' . $row['serviceStatus'] . '</td>';
                                    echo '<td><a class="btn btn-outline-success waves-effect" href="edit.php?id=' . $row['id'] . '">Edit</a></td>';
                                    echo '<td><a class="btn btn-outline-danger waves-effect" href="delete.php?id=' . $row['id'] . '">Delete</a></td></td>';
                                    echo "</tr>";
                        }
?>

        <?php
        echo $pagination->createLinks();
?>

<?php
    }
}
?>