<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include('functions.php');


if (isset($_POST['page'])) {
    //Include pagination class file
    
    include('pagination.php');
    
    
    require 'mysql.php';
    $link = mysqli_connect($host, $user, $passwd, $dbName);
    
    $start = !empty($_POST['page']) ? $_POST['page'] : 0;
    $limit = 5;
    
    //set conditions for search
    $whereSQL     = $orderSQL = '';
    $keywords     = $_POST['keywords'];
    $location     = $_POST['searchLocation'];
    $skillsSelect = $_POST['skillsSelect'];
    //$sortBy = $_POST['sortBy'];
    if (!empty($keywords) && !empty($location) && !empty($skillsSelect)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')
                                         AND location LIKE '%" . $location . "%'
                                         AND skillsReq='$skillsSelect'";
    } elseif (!empty($keywords) && !empty($location)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')
                                         AND location LIKE '%" . $location . "%'";
    } elseif (!empty($location) && !empty($skillsSelect)) {
        $whereSQL = "WHERE location='$location' 
                                         AND skillsReq='$skillsSelect'";
    } elseif (!empty($keywords) && !empty($skillsSelect)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%') 
                                         AND skillsReq='$skillsSelect'";
    } elseif (!empty($keywords)) {
        $whereSQL = "WHERE (postTitle LIKE '%" . $keywords . "%' 
                                         OR postText LIKE '%" . $keywords . "%'
                                         OR serviceType LIKE '%" . $keywords . "%')";
    } elseif (!empty($location)) {
        $whereSQL = "WHERE location LIKE '%" . $location . "%'";
    } elseif (!empty($skillsSelect)) {
        $whereSQL = "WHERE skillsReq='$skillsSelect'";
    }
    
    //    if(!empty($sortBy)){
    //        $orderSQL = " ORDER BY id ".$sortBy;
    //    }else{
    //        $orderSQL = " ORDER BY id DESC ";
    //    }
    
    //get number of rows
    $queryNum  = $link->query("SELECT COUNT(*) as postNum FROM posts $whereSQL");
    $resultNum = $queryNum->fetch_assoc();
    $rowCount  = $resultNum['postNum'];
    //initialize pagination class
    $pagConfig = array(
        'currentPage' => $start,
        'totalRows' => $rowCount,
        'perPage' => $limit,
        'link_func' => 'searchFilter'
    );
    
    $pagination = new Pagination($pagConfig);
    
    //get rows
    $query = $link->query("SELECT * FROM posts $whereSQL LIMIT $start,$limit");
    
    if ($query->num_rows > 0) {
        displayTable($query, $link);
?>

        <?php
        echo $pagination->createLinks();
?>

<?php
    }
}
?>