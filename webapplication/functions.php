
<?php
require 'mysql.php';

session_start();

// initializing variables
$username = "";
$email    = "";
$errors   = array();
$success  = array();

// connect to the database
$link = mysqli_connect($host, $user, $passwd, $dbName);
//$dashboard = "/dashboard.php";
//$currentpage = $_SERVER['REQUEST_URI'];

// REGISTER USER
if (isset($_POST['reg_user'])) {
    
    // receive all input values from the form
    $username   = mysqli_real_escape_string($link, $_POST['username']);
    $email      = mysqli_real_escape_string($link, $_POST['email']);
    $skill      = mysqli_real_escape_string($link, $_POST['skill']);
    $password_1 = mysqli_real_escape_string($link, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($link, $_POST['password_2']);
    $hashcode   = substr(md5(mt_rand()), 0, 5);
    
    // form validation: ensure that the form is correctly filled ...
    // by adding (array_push()) corresponding error unto $errors array
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    
    if (!preg_match("/^[a-zA-Z ]*$/", $username)) {
        array_push($errors, "Only letters and white space allowed as username");
    }
    
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($skill)) {
        array_push($errors, "Skill is required");
    }
    if (empty($password_1)) {
        array_push($errors, "Password is required");
    }
    if ($password_1 != $password_2) {
        array_push($errors, "Passwords do not match");
    }
    
    
    if ($_POST['captcha'] != $_SESSION['digit'])
        array_push($errors, "Sorry, the CAPTCHA code entered was incorrect!");
    
    
    // first check the database to make sure 
    // a user does not already exist with the same username and/or email
    $user_check_query = "SELECT * FROM users WHERE username='$username' OR useremail='$email' LIMIT 1";
    $result           = mysqli_query($link, $user_check_query);
    $user             = mysqli_fetch_assoc($result);
    
    if ($user) { // if user exists
        if ($user['username'] === $username) {
            array_push($errors, "Username already exists");
        }
        
        if ($user['useremail'] === $email) {
            array_push($errors, "Email already exists");
        }
    }
    
    // Finally, register user if there are no errors in the form
    if (count($errors) == 0) {
        $password = md5($password_1); //encrypt the password before saving in the database
        
        $query = "INSERT INTO users (username, useremail, userpassword, skill, hashcode) 
                VALUES('$username', '$email', '$password', '$skill', '$hashcode')";
        mysqli_query($link, $query);
        
        
        $sender    = 'ab0264b@greenwich.ac.uk';
        $recipient = $email;
        
        $subject = "Registration Confirmation Code";
        $message = "Activation code '$hashcode'";
        $headers = 'From:' . $sender;
        
        mail($recipient, $subject, $message, $headers);
        
        $_SESSION['username'] = $username;
        $_SESSION['msg']      = "You need to verify your account through email";
        header('location: verify.php');
    }
}

// USER LOGIN LOGIC

if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($link, $_POST['username']);
    $password = mysqli_real_escape_string($link, $_POST['password']);
    $remember = mysqli_real_escape_string($link, $_POST['remember']);
    
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }
    
    if (count($errors) == 0) {
        $password = md5($password);
        $query    = "SELECT * FROM users WHERE username='$username' AND userpassword='$password' AND active='1'";
        $query2   = "SELECT * FROM users WHERE username='$username' AND userpassword='$password' AND active='0'";
        $results  = mysqli_query($link, $query);
        $results2 = mysqli_query($link, $query2);
        
        
        
        if ((mysqli_num_rows($results) == 1) && ($remember == 'remember')) {
            $_SESSION['username'] = $username;
            //setting the cookie for 1 month
            $cookie_name = "user";
            $cookie_value = $username;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            
            $_SESSION['success']  = "You are now logged in";
            header('location: index.php');
        } elseif (mysqli_num_rows($results) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['success']  = "You are now logged in";
            header('location: index.php');
        } elseif (mysqli_num_rows($results2) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['msg']      = "You need to verify your account";
            header('location: verify.php');
        } else {
            array_push($errors, "Wrong username/password");
        }
        
    }
}

// USER VERIFICATION LOGIC

if (isset($_POST['verify_user'])) {
    $hash = mysqli_real_escape_string($link, $_POST['hashcode']);
    $user = $_SESSION['username'];
    
    if (empty($hash)) {
        array_push($errors, "Activation code is required");
    }
    
    if (count($errors) == 0) {
        $query  = "SELECT hashcode, active FROM users WHERE hashcode='$hash' AND active='0' AND username='$user'";
        $result = mysqli_query($link, $query);
        $match  = mysqli_num_rows($result);
        
        //echo $match;
        if ($match > 0) {
            
            $query2 = "UPDATE users SET active='1', points='100' WHERE hashcode='$hash' AND active='0' AND username='$user'";
            mysqli_query($link, $query2);
            $_SESSION['success'] = "Your account has been activated";
            header('location: index.php');
        } else {
            array_push($errors, "Activation code is invalid or you already have activated your account");
        }
    }
}

// USER POST LOGIC

if (isset($_POST['post'])) {
    
    $extension  = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $bytes      = 1024;
    $allowedKB  = 100;
    $totalBytes = $allowedKB * $bytes;
    
    // receive all input values from the form
    $postTitle   = mysqli_real_escape_string($link, $_POST['postTitle']);
    $postText    = mysqli_real_escape_string($link, $_POST['postText']);
    $serviceType = mysqli_real_escape_string($link, $_POST['serviceType']);
    $skillsReq   = mysqli_real_escape_string($link, $_POST['skillsReq']);
    $location    = mysqli_real_escape_string($link, $_POST['location']);
    $award       = mysqli_real_escape_string($link, $_POST['award']);
    //$image = mysqli_real_escape_string($link, $_FILES["files"]["tmp_name"]);
    $username    = $_SESSION['username'];
    
    // form validation: ensure that the form is correctly filled ...
    // by adding (array_push()) corresponding error unto $errors array
    if (empty($postTitle)) {
        array_push($errors, "Title is required");
    }
    if (empty($postText)) {
        array_push($errors, "Description is required");
    }
    if (empty($serviceType)) {
        array_push($errors, "Service is required");
    }
    if (empty($skillsReq)) {
        array_push($errors, "Skills are required");
    }
    if (empty($location)) {
        array_push($errors, "Location is required");
    }
    if (empty($award)) {
        array_push($errors, "Award is required");
    }
    
    
    
    if (count($errors) == 0) {
        
        $query2 = "INSERT INTO posts (username, postTitle, postText, serviceType, skillsReq, location, award, serviceStatus) VALUES('$username', '$postTitle', '$postText', '$serviceType', '$skillsReq', '$location', '$award', 'Open')";
        mysqli_query($link, $query2);
        
        //going through all images uploaded
        foreach ($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
            $uploadThisFile = true;
            
            //images information storing
            $file_name = $_FILES["files"]["name"][$key];
            $file_tmp  = $_FILES["files"]["tmp_name"][$key];
            $file_type = $_FILES["files"]["type"][$key];
            
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            
            // restrictions for file uploading
            if (!in_array(strtolower($ext), $extension)) {
                array_push($errors, "File type is invalid. Name:- " . $file_name);
                $uploadThisFile = false;
            }
            
            if ($_FILES["files"]["size"][$key] > $totalBytes) {
                array_push($errors, "File size must be less than 100KB. Name:- " . $file_name);
                $uploadThisFile = false;
            }
            
            if ($uploadThisFile) {
                
                if (!($handle = fopen($file_tmp, "r"))) {
                    die('<p>Error opening temp file</p></body></html>');
                } else if (!($image = fread($handle, filesize($file_tmp)))) {
                    die('<p>Error reading temp file</p></body></html>');
                } else {
                    fclose($handle);
                    // Commit image to the database
                    $idquery = "SELECT id FROM posts ORDER BY id DESC LIMIT 1";
                    
                    $result = mysqli_query($link, $idquery);
                    $match  = mysqli_num_rows($result);
                    if ($match > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                            $id = $row["id"];
                        }
                    }
                    $image = mysqli_real_escape_string($link, $image);
                    $alt   = htmlentities($_POST['postTitle']);
                    $query = 'INSERT INTO images (imageType,imageName,idfk,image,altText) VALUES ("' . $file_type . '","' . $file_name . '","' . $id . '","' . $image . '","' . $alt . '")';
                    //mysqli_query($link, $query);
                    if (!(mysqli_query($link, $query))) {
                        array_push($errors, "Error writing image to database");
                    } else {
                        
                        array_push($errors, "Images uploaded");
                    }
                }
            }
        }
        
        
        
        //$_SESSION['success'] = "Your post was published";
        // header('location: dashboard.php');
        
    }
}

if (isset($_POST['update'])) {
    $extension  = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $bytes      = 1024;
    $allowedKB  = 100;
    $totalBytes = $allowedKB * $bytes;
    
    $id             = mysqli_real_escape_string($link,$_POST['id']);
    $postTitle      = mysqli_real_escape_string($link, $_POST['postTitle']);
    $postText       = mysqli_real_escape_string($link,$_POST['postText']);
    $serviceType    = mysqli_real_escape_string($link,$_POST['serviceType']);
    $skillsReq      = mysqli_real_escape_string($link,$_POST['skillsReq']);
    $location       = mysqli_real_escape_string($link,$_POST['location']);
    $award          = mysqli_real_escape_string($link,$_POST['award']);
    $serviceStatus  = mysqli_real_escape_string($link,$_POST['serviceStatus']);
    
    // checking empty fields
    if (empty($postTitle)) {
        array_push($errors, "Title is required");
    }
    if (empty($postText)) {
        array_push($errors, "Description is required");
    }
    if (empty($serviceType)) {
        array_push($errors, "Service is required");
    }
    if (empty($skillsReq)) {
        array_push($errors, "Skills are required");
    }
    if (empty($location)) {
        array_push($errors, "Location is required");
    }
    if (empty($award)) {
        array_push($errors, "Award is required");
    }
    
    if (count($errors) == 0) {
        //updating the table
        $query = "UPDATE posts SET postTitle='$postTitle', postText='$postText', serviceType='$serviceType', skillsReq='$skillsReq', location='$location', award='$award', serviceStatus='$serviceStatus' WHERE id=$id";
        mysqli_query($link, $query);
        $result = mysqli_query($link, $query);
        
        //if statement does not work properly ??????
        if(isset($_FILES["files"]["tmp_name"])) {  
        foreach ($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
            $uploadThisFile = true;
            
            //images information storing
            $file_name = $_FILES["files"]["name"][$key];
            $file_tmp  = $_FILES["files"]["tmp_name"][$key];
            $file_type = $_FILES["files"]["type"][$key];
            
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            
            // restrictions for file uploading
            if (!in_array(strtolower($ext), $extension)) {
                array_push($errors, "File type is invalid. Name:- " . $file_name);
                $uploadThisFile = false;
            }
            
            if ($_FILES["files"]["size"][$key] > $totalBytes) {
                array_push($errors, "File size must be less than 100KB. Name:- " . $file_name);
                $uploadThisFile = false;
            }
            
            if ($uploadThisFile) {
                
                if (!($handle = fopen($file_tmp, "r"))) {
                    die('<p>Error opening temp file</p></body></html>');
                } else if (!($image = fread($handle, filesize($file_tmp)))) {
                    die('<p>Error reading temp file</p></body></html>');
                } else {
                    fclose($handle);
                }
                $id    = $_GET['id'];
                $image = mysqli_real_escape_string($link, $image);
                $alt   = htmlentities($_POST['postTitle']);
                $query = 'INSERT INTO images (imageType,imageName,idfk,image,altText) VALUES ("' . $file_type . '","' . $file_name . '","' . $id . '","' . $image . '","' . $alt . '")';
                //mysqli_query($link, $query);
                if (!(mysqli_query($link, $query))) {
                    array_push($errors, "Error writing image to database");
                } else {
                    
                    array_push($success, "Images uploaded");
                }
            }
        } 
        }
        
    }
} //else { header("Location: edit.php?id=".$id);}


function displayTable($result, $link)
{
    if(!isset($_SESSION['username'])) {
         echo '<div class="table-responsive">';
    echo '<table id="" class="display table">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo ' <th scope="col">#</th>';
    echo '<th scope="col">Post Title</th>';
    echo '<th scope="col">Skills Required</th>';
    echo '<th scope="col">Service Status</th>';
    echo '<th scope="col">View</th>';
    echo ' </tr>';
    echo '</thead>';
    
    
    while ($row = mysqli_fetch_array($result)) {
        $id        = $row['id'];
        $imgquery  = "SELECT id, imageName, altText FROM images WHERE idfk=$id LIMIT 1";
        $imgresult = mysqli_query($link, $imgquery);
        
        echo '<tr >';
        echo '<th scope="row">' . $row['id'] . '</th>';
        if (!isset($_SESSION['username'])) {
        echo '<td><a href="post-single.php?id=' . $row['id'] . '">' . $row['postTitle'] . '</a></td>';
        } else {
            echo '<td>' . $row['postTitle'] . '</td>';
        }
        echo '<td>' . $row['skillsReq'] . '</td>';
        echo '<td>' . $row['serviceStatus'] . '</td>';
        if (!isset($_SESSION['username'])) {
            // using popover.js from bootstrap for an error if users are not logged in
            echo '<td>
                                        <button data-trigger="hover" data-toggle="popover" data-placement="top" data-content="You must be logged in to see the full post." type="submit" class="btn btn-outline-warning waves-effect poper" name="notloggedin">View</button></td>';
        } else {
            echo '<td><a class="btn btn-outline-warning waves-effect" href="post-single.php?id=' . $row['id'] . '">View</a></td>';
        }
        echo "</tr>";
    }
    echo '</table></div>';
    } else {
    echo '<div class="table-responsive">';
    echo '<table id="" class="display table">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo ' <th scope="col">#</th>';
    echo '<th scope="col">Post Title</th>';
    echo '<th scope="col">Post Image</th>';
    echo '<th scope="col">Post Text</th>';
    echo '<th scope="col">Service Type</th>';
    echo '<th scope="col">Skills Required</th>';
    echo '<th scope="col">Location</th>';
    echo '<th scope="col">Points award</th>';
    echo '<th scope="col">Service Status</th>';
    echo '<th scope="col">View</th>';
    echo ' </tr>';
    echo '</thead>';
    
    
    while ($row = mysqli_fetch_array($result)) {
        $id        = $row['id'];
        $imgquery  = "SELECT id, imageName, altText FROM images WHERE idfk=$id LIMIT 1";
        $imgresult = mysqli_query($link, $imgquery);
        
        echo '<tr >';
        echo '<th scope="row">' . $row['id'] . '</th>';
        if (!isset($_SESSION['username'])) {
        echo '<td><a href="post-single.php?id=' . $row['id'] . '">' . $row['postTitle'] . '</a></td>';
        } else {
            echo '<td>' . $row['postTitle'] . '</td>';
        }
        echo '<td>';
        for ($i = 0; $i < mysqli_num_rows($imgresult); $i++) {
            $imgrow = mysqli_fetch_assoc($imgresult);
            echo '<img src="getImage.php?id=' . $imgrow['id'] . '" alt="' . $imgrow['altText'] . '" title="' . $imgrow['imageName'] . '" width="80" height="80"/>  ' . "\n";
        }
        echo '</td>';
        echo '<td>' . $row['postText'] . '</td>';
        echo '<td>' . $row['serviceType'] . '</td>';
        echo '<td>' . $row['skillsReq'] . '</td>';
        echo '<td>' . $row['location'] . '</td>';
        echo '<td>' . $row['award'] . '</td>';
        echo '<td>' . $row['serviceStatus'] . '</td>';
        if (!isset($_SESSION['username'])) {
            // using popover.js from bootstrap for an error if users are not logged in
            echo '<td>
                                        <button data-trigger="hover" data-toggle="popover" data-placement="top" data-content="You must be logged in to see the full post." type="submit" class="btn btn-outline-warning waves-effect poper" name="notloggedin">View</button></td>';
        } else {
            echo '<td><a class="btn btn-outline-warning waves-effect" href="post-single.php?id=' . $row['id'] . '">View</a></td>';
        }
        echo "</tr>";
    }
    echo '</table></div>';
}}




?>