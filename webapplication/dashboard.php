<?php error_reporting(E_ALL); ini_set('display_errors', 'On'); 
//Include pagination class file
      
      include('functions.php'); 
    ?>
<?php
    require 'mysql.php';
    $link = mysqli_connect($host, $user, $passwd, $dbName);
    
    
if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }?>
<?php include('header-fluid.php') ?>
   
                        <h5 class="card-title text-center">Dashboard</h5>
                        <?php 
                                include('errors.php'); ?>
                        <div class="row">
                            <div class="col-sm-4 margintopbot">
                                    <input class="form-control" id="keywords" type="text" placeholder="Search" onkeyup="searchFilter()">
                            </div>
                            <div class="col-sm-4 margintopbot">
                                <input class="form-control" id="searchLocation" type="text" placeholder="Search by location" onkeyup="searchFilter()">
                            </div>
                            <div class="col-sm-4 margintopbot">
                                <div class="form-check">
                                    
                                    <select class="form-control" id="skillsSelect" onchange="searchFilter()">
                                        <option value="" selected="selected" >Search by skill</option>
                                        <?php
                                            $sel_query="SELECT skillsReq FROM posts ORDER BY id asc";
                                            $result = mysqli_query($link,$sel_query);

                                            while($row = mysqli_fetch_array($result)) {
                                                echo '<option value="'.$row['skillsReq'].'">' .$row['skillsReq']. '</option>';  
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            </div></br>
                            <div id="posts_content" class="row">
                            
                                <?php
                                //Include pagination class file
                                include('pagination.php');

                                $limit = 5;

                                //get number of rows
                                $query="SELECT COUNT(*) as postNum FROM posts ";
                                $queryNum = mysqli_query($link,$query);
                                $resultNum = mysqli_fetch_assoc($queryNum);
                                $rowCount = $resultNum['postNum'];
                                //echo $rowCount;

                                //initialize pagination class
                                $pagConfig = array(
                                    'totalRows' => $rowCount,
                                    'perPage' => $limit,
                                    'link_func' => 'searchFilter'
                                );
                                $pagination =  new Pagination($pagConfig);

                                //get rows
                                $query2="SELECT * FROM posts ORDER BY id DESC LIMIT $limit";
                                $result2=mysqli_query($link,$query2);

                                if(mysqli_num_rows($result2) > 0){ ?>
                                    <?php 
                                        displayTable($result2,$link);
                                        echo $pagination->createLinks(); ?>
                                <?php } ?>
</div>

<?php include('footer.php') ?>