<?php include('functions.php') ?>
<?php
  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first to authenticate your user";
  	header('location: login.php');
  }

?>
<?php include('header.php') ?>
                        <h5 class="card-title text-center">Activate your account</h5>
                        <!-- notification message -->
                                <?php if (isset($_SESSION['success'])) : ?>
                                <div class="alert alert-danger">
                                    <span>
                                      <?php 
                                        echo $_SESSION['msg']; 
                                        unset($_SESSION['msg']);
                                      ?>
                                    </span>
                                </div>
                                    <?php endif ?>
                        <form class="form-signin" method="post" action="verify.php">
                            <?php include('errors.php'); ?>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="hashcode" placeholder="Activation Code">
                            </div>
                            <div class="form-label-group">
                                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" name="verify_user">Verify</button>
                            </div>
                            <p>
                                Not yet a member? <a href="register.php">Sign up</a>
                            </p>
                        </form>
<?php include('footer.php') ?>