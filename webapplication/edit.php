<?php include('functions.php') ?>
<?php

  
    
  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first to edit your post";
  	header('location: login.php');
  } else {
      $username = $_SESSION['username'];
      $query = "SELECT active FROM users WHERE username='$username' AND active='1'";
      $result = mysqli_query($link, $query);
      $res = mysqli_fetch_assoc($result);
      $active = $res['active'];
      if($active!=1) {
          $_SESSION['msg'] = "You must verify your account first to edit your post";
  	      header('location: verify.php');
      } 
      
  }
//getting id from url
$id = $_GET['id'];
$username = $_SESSION['username'];
$_SESSION['edit'] = $id;
 
//selecting data associated with this particular id
$query2 = "SELECT id FROM posts WHERE username='$username'";
$result2 = mysqli_query($link, $query2);
$res2 = mysqli_fetch_assoc($result2);
$idMatch = $res2['id'];

  if($id != $idMatch) {
    $_SESSION['msg'] = "404 - Page Not Found";
  	header('location: myposts.php');
  }

  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }

$query = "SELECT * FROM posts WHERE id=$id";
$result = mysqli_query($link, $query);
 
while($res = mysqli_fetch_array($result))
{
    $id = $res['id'];
    $imgquery = "SELECT id, imageName, altText FROM images WHERE idfk=$id";
    $imgresult = mysqli_query($link,$imgquery);
    $postTitle = $res['postTitle'];
    $postText = $res['postText'];
    $serviceType = $res['serviceType'];
    $skillsReq = $res['skillsReq'];
    $location = $res['location'];
    $award = $res['award']; 
    $serviceStatus = $res['serviceStatus'];
    
}

?>

<?php include('header.php') ?>

                        <h5 class="card-title text-center">Edit the post</h5>
                        <form class="form-signin" method="post" action="edit.php?id=<?php echo $_GET['id'];?>" enctype="multipart/form-data">
                            <?php include('errors.php'); ?>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="postTitle" placeholder="Post Title" value="<?php echo $postTitle; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="postText" placeholder="Post description" value="<?php echo $postText; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="serviceType" placeholder="Plumbing, programming etc." value="<?php echo $serviceType; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="skillsReq" placeholder="Skills required" value="<?php echo $skillsReq; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="location" placeholder="Location" value="<?php echo $location; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="award" placeholder="Points award for service. ex. '100'" value="<?php echo $award; ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Post status. Now: </label><?php echo $res['serviceStatus']; ?>
                                <select class="form-control" id="serviceStatus" name="serviceStatus">
                                  <option value="Open">Open</option>
                                  <option value="Completed">Completed</option>
                                </select>
                              </div>
                            <div class="form-label-group">
                                Select Image Files to Upload:
                                <input type="file" name="files[]" multiple >
                            
                            </div>
                            <div class="form-label-group">
                                <div class="row">
                                <?php 
                                      for ( $i = 0 ; $i < mysqli_num_rows($imgresult) ; $i++ ) {
                                        $imgrow = mysqli_fetch_assoc($imgresult);
                                        echo '<div class="col-sm-4"><img class="img-thumbnail" src="getImage.php?id=' . $imgrow['id'] . '" alt="' . $imgrow['altText'] . '" title="' . $imgrow['imageName']  .'" /><a class="btn btn-outline-danger waves-effect" href="delete.php?imgid=' . $imgrow['id'] . '">Delete</a></div>  ';
                                      }
                                ?>
                            </div></div>
                            <div class="form-label-group">
                                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" name="update">Update</button>
                            </div>
                        </form>
<?php include('footer.php') ?>