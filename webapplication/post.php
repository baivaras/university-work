<?php include('functions.php') ?>
<?php
  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first to post";
  	header('location: login.php');
  }

  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }

?>
<?php include('header.php') ?>
                        <h5 class="card-title text-center">Post a Job</h5>
                        <form class="form-signin" method="post" action="post.php" enctype="multipart/form-data">
                            <?php include('errors.php'); ?>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="postTitle" placeholder="Post Title" value="<?php echo $postTitle; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="postText" placeholder="Post description" value="<?php echo $postText; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="serviceType" placeholder="Plumbing, programming etc." value="<?php echo $serviceType; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="skillsReq" placeholder="Skills required" value="<?php echo $skillsReq; ?>">
                            </div>
                            <div class="form-label-group">
<!--                                <input id="pac-input" class="form-control controls" type="text" placeholder="Location">-->
                                <input class="form-control" type="text" name="location" placeholder="Location" value="<?php echo $location; ?>">
                            </div>
                            <div class="form-label-group">
                                <input class="form-control" type="text" name="award" placeholder="Points award for service. ex. '100'" value="<?php echo $award; ?>">
                            </div>
                            <div class="form-label-group">
                                Select Image Files to Upload:
                                <input type="file" name="files[]" multiple >
                            
                            </div>
                            <div class="form-label-group">
                                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" name="post">Post</button>
                            </div>

                            <p>
                                Already posted? <a href="index.php">Go to Home Page</a>
                            </p>
                        </form>
<?php include('footer.php') ?>