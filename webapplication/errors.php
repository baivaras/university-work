<?php  if (count($errors) > 0) : ?>
  <div class="alert alert-danger">
  	<?php foreach ($errors as $error) : ?>
  	  <p><?php echo $error ?></p>
  	<?php endforeach ?>
  </div>
<?php  endif ?>

<?php  if (count($success) > 0) : ?>
  <div class="alert alert-success">
  	<?php foreach ($success as $succes) : ?>
  	  <p><?php echo $succes ?></p>
  	<?php endforeach ?>
  </div>
<?php  endif ?>