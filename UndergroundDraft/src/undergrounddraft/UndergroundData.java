package undergrounddraft;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UndergroundData {

    //Class to hold the tree map of all the stations and their connection
    private static class Item {

        //These are the column headings for the tree map as well as an ID column
        //The different connection columns of are the potential of having 10 differnent stations connected to one
        private String name;
        private String connection1;
        private int connection1Length;
        private String connection1Line;
        private String connection2;
        private int connection2Length;
        private String connection2Line;
        private String connection3;
        private int connection3Length;
        private String connection3Line;
        private String connection4;
        private int connection4Length;
        private String connection4Line;
        private String connection5;
        private int connection5Length;
        private String connection5Line;
        private String connection6;
        private int connection6Length;
        private String connection6Line;
        private String connection7;
        private int connection7Length;
        private String connection7Line;
        private String connection8;
        private int connection8Length;
        private String connection8Line;

        //This defines the parameters of an item in the map in this case all the charaistics of a station
        Item(String n, String c1, int c1Le, String c1Li, String c2, int c2Le, String c2Li, String c3, int c3Le, String c3Li, String c4, int c4Le, String c4Li, String c5, int c5Le, String c5Li, String c6, int c6Le, String c6Li, String c7, int c7Le, String c7Li, String c8, int c8Le, String c8Li) {
            //These are the variables holding the data from the map and which column of the map they are assigned to
            name = n;
            connection1 = c1;
            connection1Length = c1Le;
            connection1Line = c1Li;
            connection2 = c2;
            connection2Length = c2Le;
            connection2Line = c2Li;
            connection3 = c3;
            connection3Length = c3Le;
            connection3Line = c3Li;
            connection4 = c4;
            connection4Length = c4Le;
            connection4Line = c4Li;
            connection5 = c5;
            connection5Length = c5Le;
            connection5Line = c5Li;
            connection6 = c6;
            connection6Length = c6Le;
            connection6Line = c6Li;
            connection7 = c7;
            connection7Length = c7Le;
            connection7Line = c7Li;
            connection8 = c8;
            connection8Length = c8Le;
            connection8Line = c8Li;
        }
    }
    //the below code reads the train data from the txt file and creates a tree map

    //The defining and initialsing of the tree map where all the station data is stored 
    private static final Map<String, Item> library = new TreeMap();
    //Each entry into the map is separate station

    static {
        String appDir = System.getProperty("user.dir");
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(appDir + "/trainData.txt");
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ArrayList<String> trainStations = new ArrayList();
            String station = "";
            String[] connections = new String[8];
            int[] connectionLengths = new int[8];
            String[] lines = new String[8];
            int i = 0;
            //Each entry into the map is separate station
            while (!"x".equals(station)) {
                try {
                    station = bufferedReader.readLine();
                    if (!"x".equals(station)) {
                        trainStations.add(station);
                        for (int j = 0; j < 8; j++) {
                            connections[j] = bufferedReader.readLine();
                            connectionLengths[j] = Integer.parseInt(bufferedReader.readLine());
                            lines[j] = bufferedReader.readLine();
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(UndergroundDraft.class.getName()).log(Level.SEVERE, null, ex);
                }
                library.put(String.valueOf(i), new Item(station, connections[0], connectionLengths[0], lines[0], connections[1], connectionLengths[1], lines[1], connections[2], connectionLengths[2], lines[2], connections[3], connectionLengths[3], lines[3], connections[4], connectionLengths[4], lines[4], connections[5], connectionLengths[5], lines[5], connections[6], connectionLengths[6], lines[6], connections[7], connectionLengths[7], lines[7]));
                i++;
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(UndergroundDraft.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //returns name of station
    public static String getName(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.name;
        }
    }

    //returns key of station
    public static String getKey(String name, ArrayList<String> trainStations) {
        String key = "";
        for (int j = 0; j < trainStations.size(); j++) {
            if ((trainStations.get(j)).equals(name)) {
                key = String.valueOf(j);
                break;
            }
        }
        return key;
    }

    //returns connection 1
    public static String getConnection1(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection1;
        }
    }
    //returns connection 2

    public static String getConnection2(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection2;
        }
    }
    //returns connection 3

    public static String getConnection3(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection3;
        }
    }
    //returns connection 4

    public static String getConnection4(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection4;
        }
    }
    //returns connection 5

    public static String getConnection5(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection5;
        }
    }
    //returns connection 6

    public static String getConnection6(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection6;
        }
    }
    //returns connection 7

    public static String getConnection7(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection7;
        }
    }
    //returns connection 8

    public static String getConnection8(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection8;
        }
    }
    //returns connection 1 line

    public static String getConnection1Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection1Line;
        }
    }
    //returns connection 2 line

    public static String getConnection2Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection2Line;
        }
    }
    //returns connection 3 line

    public static String getConnection3Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection3Line;
        }
    }
    //returns connection 4 line

    public static String getConnection4Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection4Line;
        }
    }
    //returns connection 5 line

    public static String getConnection5Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection5Line;
        }
    }
    //returns connection 6 line

    public static String getConnection6Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection6Line;
        }
    }
    //returns connection 7 line

    public static String getConnection7Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection7Line;
        }
    }
    //returns connection 8 line

    public static String getConnection8Line(String key) {
        Item item = library.get(key);
        if (item == null) {
            return null; // null means no such item
        } else {
            return item.connection8Line;
        }
    }
    //returns connection 1 length

    public static int getConnectionLength1(String key) {
        Item item = library.get(key);
        return item.connection1Length;
    }
    //returns connection 2 length

    public static int getConnectionLength2(String key) {
        Item item = library.get(key);
        return item.connection2Length;
    }
    //returns connection 3 length

    public static int getConnectionLength3(String key) {
        Item item = library.get(key);
        return item.connection3Length;
    }
    //returns connection 4 length

    public static int getConnectionLength4(String key) {
        Item item = library.get(key);
        return item.connection4Length;
    }
    //returns connection 5 length

    public static int getConnectionLength5(String key) {
        Item item = library.get(key);
        return item.connection5Length;
    }
    //returns connection 6 length

    public static int getConnectionLength6(String key) {
        Item item = library.get(key);
        return item.connection6Length;
    }
    //returns connection 7 length

    public static int getConnectionLength7(String key) {
        Item item = library.get(key);
        return item.connection7Length;
    }
    //returns connection 8 length

    public static int getConnectionLength8(String key) {
        Item item = library.get(key);
        return item.connection8Length;
    }

}
