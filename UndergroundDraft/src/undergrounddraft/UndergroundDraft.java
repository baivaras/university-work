//The following youtube video should be credited for helping understand java tables:
//https://www.youtube.com/watch?v=22MBsRYuM4Q
//The following wesite should be credited for helping with row colours depending on line:
//https://tips4java.wordpress.com/2010/01/24/table-row-rendering/
package undergrounddraft;
//Imports needed for the program

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

//class that holds the code for the program
public class UndergroundDraft {

    ArrayList<String> trainStations = fillStations();
    //Defining GUI components to be used later
    //All of these gui components are global variables
    JFrame frame = new JFrame();
    JPanel form = new JPanel();
    JPanel tablePanel = new JPanel();
    //JTable table = new JTable();
    JTable table = new JTable();
    JTextField startStation = new JTextField(15);
    JTextField endStation = new JTextField(15);
    JTextField departureTimeHours = new JTextField(5);
    JTextField departureTimeMinutes = new JTextField(5);
    JTextField arrivalTimeHours = new JTextField(5);
    JTextField arrivalTimeMinutes = new JTextField(5);
    TextArea journeySummary = new TextArea(10, 70);
    JButton enter = new JButton("Enter");
    JButton help = new JButton("?");
    //Object used to store data to be stored in the table rows
    Object[] row = new Object[4];
    //TableModel is needed to create a table and is defined as global so that it can be called later when data is displayed in the table
    DefaultTableModel tableModel = new DefaultTableModel();

    //stores the last station visited by each of the trainStations.size() stations while the algorithm in implemented
    //this allows the program to "trace backwards" through the last stations to find the route
    String[] lastStationVisited = new String[trainStations.size()];
    //stores the accumulating shortest time to reach each station
    //ie. in the index matching the final station the overall time for the whole journey will be stored
    int[] totalTimeForEachStation = new int[trainStations.size()];
    //stores the line taken from the last staion to get to the each station in the shortest time
    String[] lineFromLastStation = new String[trainStations.size()];
    //stores a 1 or 0 representing whether the station is permanent 
    //if the station is permanent then it cannot be visited again
    int[] permanentStations = new int[trainStations.size()];
    //stores half of the time (hours) for each station
    int[] hours = new int[trainStations.size()];
    //stores the other half of the time (minutes) for each station
    //put these two together with the same index - and the time is displayed at which the user will arrive at each station
    int[] minutes = new int[trainStations.size()];
    //global variables for the user input
    String start;
    String end;
    //stores the next station to be reviewed allowing for the program to continue to loop the same code without repitition of code
    String nextStation;

    //Method to build the GUI
    public UndergroundDraft() throws IOException {
        //Setting the characteristics of the form
        frame.setTitle("London Ungerground Route Planner");
        frame.setSize(600, 850);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.add(form);
        //Displaying the title at the top of the form
        JPanel title = new JPanel();
        JLabel label1 = new JLabel();
        label1.setFont(new Font("Serif", Font.BOLD, 35));
        label1.setText("London Underground Route Finder");
        title.add(label1);
        form.add(title);
        //creating the textboxes and labels for user input
        JPanel input = new JPanel();
        JLabel label3 = new JLabel("Starting Station: ");
        JLabel label4 = new JLabel("Ending Station: ");
        input.add(label3);
        input.add(startStation);
        input.add(label4);
        input.add(endStation);
        form.add(input);
        JPanel input1 = new JPanel();
        JLabel label5 = new JLabel("Leaving Time: ");
        JLabel label6 = new JLabel("Arrival Time: ");
        JLabel label7 = new JLabel(":");
        JLabel label8 = new JLabel(":");
        input1.add(label5);
        input1.add(departureTimeHours);
        input1.add(label7);
        input1.add(departureTimeMinutes);
        input1.add(label6);
        input1.add(arrivalTimeHours);
        input1.add(label8);
        input1.add(arrivalTimeMinutes);
        form.add(input1);
        form.add(enter);
        form.add(help);
        //creating the table and displaying that under the title

        Object[] columns = {"Station", "Line", "Time to Next Station", "Total Time"};
        tableModel.setColumnIdentifiers(columns);
        table.setModel(tableModel);
        tableModel.isCellEditable(0, 0);

        //setting preferred column width for each column
        //adding a scroll on the side of the table incase the data is too large
        tablePanel.add(createData(tableModel));
        form.add(tablePanel);
        frame.setVisible(false);
        frame.setVisible(true);
        //creating the journey summary part of the form
        JPanel journeySummaryTitle = new JPanel();
        JLabel label2 = new JLabel();
        label2.setFont(new Font("Serif", Font.BOLD, 20));
        label2.setText("Journey Summary:");
        journeySummaryTitle.add(label2);
        form.add(journeySummaryTitle);
        JPanel journeySummaryPanel = new JPanel();
        journeySummaryPanel.add(journeySummary);
        journeySummary.setEditable(false);
        form.add(journeySummaryPanel);
        frame.setVisible(false);
        frame.setVisible(true);
        //adding action listener to the enter button
        ButtonHandler phandler = new ButtonHandler();
        enter.addActionListener(phandler);
        help.addActionListener(phandler);
    }

    //Class that dictates what happens when an event occurs
    class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            //check if the enter button was pressed
            //this is where the methods will be called
            if ((e.getSource() == enter)) {
                //clear the arrays for use again without resetting the program
                for (int i = 0; i < trainStations.size(); i++) {
                    permanentStations[i] = 0;
                    lastStationVisited[i] = "";
                    lineFromLastStation[i] = "";
                    totalTimeForEachStation[i] = 0;
                }
                //clears the text area and table
                journeySummary.setText("");
                tableModel.setRowCount(0);
                //store the entered stations into global variables
                start = startStation.getText();
                end = endStation.getText();

                //validation displays an error message if one of the inputs is wrong
                if (!departureTimeHours.getText().matches("[0-9]+") || !departureTimeMinutes.getText().matches("[0-9]+") || "".equals(departureTimeHours.getText()) || "".equals(departureTimeHours.getText()) || "".equals(UndergroundData.getKey(start, trainStations)) || "".equals(UndergroundData.getKey(end, trainStations)) || start.equals(end) || Integer.parseInt(departureTimeHours.getText()) > 23 || Integer.parseInt(departureTimeMinutes.getText()) > 59 || Integer.parseInt(departureTimeHours.getText()) < 05) {
                    JOptionPane.showMessageDialog(null, "There is a problem with one or more of your inputs. Please try again");
                } else {
                    //stores the entered start time
                    int departHour = Integer.parseInt(departureTimeHours.getText());
                    int departMins = Integer.parseInt(departureTimeMinutes.getText());
                    //stores the start time in the time index for every station
                    for (int i = 0; i < trainStations.size(); i++) {
                        hours[i] = departHour;
                        minutes[i] = departMins;
                    }
                    //sets the next station to review as the starting station
                    nextStation = start;
                    //loop to check dijkstras algorithm is complete
                    boolean complete = false;
                    while (complete != true) {
                        //method containing the main algorithm code
                        ExamineNodesAdjacent();
                        //method checks whether the algorithm is finished
                        complete = CheckIfComplete();
                    }
                    String done = "false";
                    //displays the arrival time
                    arrivalTimeHours.setText(String.valueOf(hours[Integer.parseInt(UndergroundData.getKey(end, trainStations))]));
                    arrivalTimeMinutes.setText(String.valueOf(minutes[Integer.parseInt(UndergroundData.getKey(end, trainStations))]));
                    if (Integer.parseInt(arrivalTimeMinutes.getText()) < 10) {
                        arrivalTimeMinutes.setText("0" + arrivalTimeMinutes.getText());
                    }
                    if (Integer.parseInt(arrivalTimeHours.getText()) < 10) {
                        arrivalTimeHours.setText("0" + arrivalTimeHours.getText());
                    }
                    //begins the process of working backwards through the dijkstra algorithm to find the route
                    String currentStation = end;
                    int currentStationKey;
                    int previousStationKey;
                    int totalTime = 0;
                    String currentLine;
                    //these array lists are used because they are endless and easy to reverse at the end of this process
                    ArrayList<String> journey = new ArrayList();
                    ArrayList<String> lines = new ArrayList();
                    ArrayList<Integer> times = new ArrayList();
                    //store the keys for the end station and the second to last station
                    currentStationKey = Integer.parseInt(UndergroundData.getKey(currentStation, trainStations));
                    previousStationKey = Integer.parseInt(UndergroundData.getKey(lastStationVisited[currentStationKey], trainStations));
                    //while loop fills the array list with the journey, times and lines
                    //however, becuase the journey started at the end station, it is in the reverse order
                    while (done == "false") {
                        if (currentStation == start) {
                            journey.add(start);
                            done = "true";
                        } else {
                            currentStationKey = Integer.parseInt(UndergroundData.getKey(currentStation, trainStations));
                            previousStationKey = Integer.parseInt(UndergroundData.getKey(lastStationVisited[currentStationKey], trainStations));
                            journey.add(currentStation);
                            lines.add(lineFromLastStation[currentStationKey]);
                            int timeDiff = totalTimeForEachStation[currentStationKey] - totalTimeForEachStation[previousStationKey];
                            times.add(timeDiff);
                            currentStation = lastStationVisited[currentStationKey];
                        }
                    }
                    //reverse the array lists so that they are all in the correct order
                    Collections.reverse(journey);
                    Collections.reverse(lines);
                    Collections.reverse(times);
                    //at empty entries into the time and lines because the end station will not have a line and time to the next station
                    //also keeps the sizes of the array lists the same
                    lines.add("");
                    times.add(0);
                    //this loop displays the data into the table
                    //uses the global object row to display the data according to the column numbers
                    for (int i = 0; i < journey.size(); i++) {
                        if ("Bakerloo".equals(lines.get(i))) {

                        } else {
                        }
                        row[0] = journey.get(i);
                        if (journey.get(i) == end) {
                            row[1] = "";
                            row[2] = 0;
                            row[3] = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(end, trainStations))];
                        } else {
                            row[1] = lines.get(i);
                            row[2] = times.get(i);
                            totalTime += times.get(i);
                            row[3] = totalTime;
                        }
                        //adds the current row and allows for a new row to be created instantly

                        tableModel.addRow(row);
                    }

                    //displays the journey summary using the reversed array lists
                    String finish = "false";
                    //displays the start station and the starting "line to next station"
                    journeySummary.append("Take the: " + lines.get(0) + " line from: " + journey.get(0) + "\n");
                    int index = 0;
                    int index2 = 0;
                    //This loop counts until the user needs to change line
                    //then stores the position as to where the line change is
                    //this position will show the station at which they need to change

                    while (finish.equals("false")) {
                        for (int i = 0; i < lines.size() - 1; i++) {
                            if (lines.get(i + 1).equals("")) {
                                finish = "true";
                                index2 = i;
                            } else if (!lines.get(i).equals(lines.get(i + 1))) {
                                index = i;
                                journeySummary.append("To: " + journey.get(index + 1) + "\n");
                                journeySummary.append("Possible change - you might already be on the next line" + "\n");
                                journeySummary.append("Take the: " + lines.get(index + 1) + " line from: " + journey.get(index + 1) + "\n");
                            }

                        }

                        journeySummary.append("To: " + end + "\n");
                        journeySummary.append("Your journey will take: " + totalTime + " minutes");

                    }

                }

            } else {
                //help button message
                JOptionPane.showMessageDialog(null, "Welcome to the help page\nEnter your start and finish stations in the textboxes\nIf there are problems, make sure all apostrophes and brackets are removed\nIf there are time problems, remember that stations only open from 5am till midnight\nThe lines coloured white signify a choice of line that can be made by you, please keep the other lines in mind when making a decision\nThank You!");

            }
        }
    }

    public ArrayList<String> fillStations() {
        try {
            String appDir = System.getProperty("user.dir");
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(appDir + "/trainData.txt");
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ArrayList<String> trainStations1 = new ArrayList();
            String station = "";
            //Each entry into the map is separate station
            while (!"x".equals(station)) {
                station = bufferedReader.readLine();
                if (!"x".equals(station)) {
                    trainStations1.add(station);
                    for (int k = 0; k < 24; k++) {
                        bufferedReader.readLine();
                    }
                }
            }
            return trainStations1;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            Logger.getLogger(UndergroundData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    //This method is used to check if the user is on the bakerloo line during peak times
    public String checkIfOnBakerloo(String line, String key) {
        //stores the current hour for the current station
        int hour = hours[Integer.parseInt((key))];
        //if it within the peak time and is on the bakerloo return true
        if ((hour >= 9) && (hour < 16)) {
            //This line means that even if the train says bakerloo or nothern, the bakerloo part will be discovered
            if (line.toLowerCase().indexOf("Bakerloo".toLowerCase()) != -1) {
                return "true";
            }
        }
        //if it within the peak time and is on the bakerloo return true
        if ((hour >= 19) && (hour < 24)) {
            //This line means that even if the train says bakerloo or nothern, the bakerloo part will be discovered
            if (line.toLowerCase().indexOf("Bakerloo".toLowerCase()) != -1) {
                return "true";
            }
        }
        //else it returns false meaning that the train is not on the bakerloo line
        return "false";
    }

    //this method is used to record the time for each station
    public void Time(int time, int key) {
        //increases the minutes
        minutes[(key)] += time;
        //converts 60 minutes into 1 hour
        while (minutes[(key)] > 59) {
            minutes[(key)] = (minutes[(key)] - 59);
            hours[(key)] += 1;
            if (hours[(key)] > 23) {
                hours[(key)] = 00;
            }
        }
    }

    //This method determines whether the algorithm in finished
    //if the permanent staion = 1 then it is true meaning that that station is finished with the algorithm
    //if we dont check this often then uneeded stations will be checked
    public boolean CheckIfComplete() {
        String key = UndergroundData.getKey(end, trainStations);
        for (int i = 0; i < trainStations.size(); i++) {
            if (permanentStations[i] == 0) {
                return false;
            }
        }
        if (permanentStations[Integer.parseInt(key)] == 1) {
            if (hours[Integer.parseInt(key)] < 05) {
                //displays the error message
                JOptionPane.showMessageDialog(null, "This journey will reach beyond closure times for the London Underground. Please plan your journey to finish before midnight");
            }
            return true;
        } else {
            return false;
        }
    }

    //this methos checks if the train has changed lines
    //need this method because connections of the same length to the same station are paired (bakerloo or northern ...)
    public boolean CheckForLineChangeTwo(String key, String line) {
        boolean check = true;
        //checks if the lines are both bakerloo
        if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Bakerloo".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Bakerloo".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Central
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Central".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Central".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Circle
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Circle".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Circle".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both District
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("District".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("District".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Hammersmith & City
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Hammersmith".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Hammersmith".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Jubilee
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Jubilee".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Jubilee".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Metropolitan
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Metropolitan".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Metropolitan".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Northern
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Northern".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Northern".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Piccadilly
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Piccadilly".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Piccadilly".toLowerCase()) != -1) {
                check = false;
            }
            //checks if the lines are both Victoria
        } else if (lineFromLastStation[Integer.parseInt(key)].toLowerCase().indexOf("Victoria".toLowerCase()) != -1) {
            if (line.toLowerCase().indexOf("Victoria".toLowerCase()) != -1) {
                check = false;
            }
        }
        return check;
    }

    //This method stores all the stations that connect to the start
    public void ExamineNodesAdjacent() {
        String key = UndergroundData.getKey(nextStation, trainStations);//get the key
        permanentStations[Integer.parseInt(key)] = 1;//make station permanent
        int[] adjacentNodes = {UndergroundData.getConnectionLength1(key), UndergroundData.getConnectionLength2(key), UndergroundData.getConnectionLength3(key), UndergroundData.getConnectionLength4(key), UndergroundData.getConnectionLength5(key), UndergroundData.getConnectionLength6(key), UndergroundData.getConnectionLength7(key), UndergroundData.getConnectionLength8(key)};

        //updating last stations and times for all stations attatched
        String bakerlooCheck = "false";

        //the below comments are for the next 400 lines
        //This section of the code is the dyjkstras algorithm
        //The code is repeated for different connections 1 - 8
        //This is because the station with the most connections is Kings Cross with 8
        //The adjacentNode[] > 0 is there so that if there is no connection (ie. a station has only 1 connection, connections 2-8 will be 0) the code will be bypassed and time saved
        //The times added are due to many reasons
        // +1 while the train waits at the station
        // +5 if they need to change line
        // /2 if they are on the bakerloo line
        //The statements at the bot tom of each mean that if the original time was longer than the new calculated time, the new time is saved, otherwise the old time is kept
        //Check for line change is there for the connections that occur on different lines but take the same time they are combined therefore will needed to be tested to see if the user will need to change line
        if (adjacentNodes[0] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection1(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection1Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection1Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength1(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength1(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection1Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection1Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength1(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength1(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection1Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength1(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength1(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection1Line(key);
                        }

                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection1Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[1] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection2(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection2Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection2Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength2(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength2(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection2Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection2Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength2(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength2(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection2Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength2(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength2(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection2Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection2Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[2] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection3(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection3Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection3Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength3(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength3(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection3Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection3Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength3(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength3(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection3Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength3(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength3(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection3Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection3Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[3] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection4(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection4Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection4Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength4(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength4(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection4Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection4Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength4(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength4(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection4Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength4(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength4(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection4Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection4Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[4] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection5(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection5Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection5Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength5(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength5(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection5Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection5Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength5(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength5(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection5Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength5(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength5(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection5Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection5Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[5] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection6(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection6Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection6Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength6(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength6(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection6Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection6Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength6(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength6(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection6Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength6(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength6(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection6Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection6Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[6] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection7(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection7Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection7Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength7(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength7(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection7Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection7Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength7(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength7(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection7Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength7(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength7(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection7Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection7Line(key);
                    }
                }

            }
        }

        if (adjacentNodes[7] > 0) {
            int adjacentKey = Integer.parseInt(UndergroundData.getKey(UndergroundData.getConnection8(key), trainStations));
            if (permanentStations[adjacentKey] == 0) {

                int originalLength = totalTimeForEachStation[Integer.parseInt(UndergroundData.getKey(nextStation, trainStations))];
                int originalAdjacentLength = totalTimeForEachStation[adjacentKey];
                int newLength = 0;
                if ((lineFromLastStation[Integer.parseInt(key)] == UndergroundData.getConnection8Line(key)) || (lineFromLastStation[Integer.parseInt(key)] == "")) {
                    bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection8Line(key), key);
                    if (bakerlooCheck == "true") {
                        newLength += ((UndergroundData.getConnectionLength8(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    } else {
                        newLength += UndergroundData.getConnectionLength8(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                    }
                } else {
                    boolean check = CheckForLineChangeTwo(String.valueOf(key), UndergroundData.getConnection8Line(key));
                    if (check == true) {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection8Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength8(key)) / 2) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength8(key) + 6 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    } else {
                        bakerlooCheck = checkIfOnBakerloo(UndergroundData.getConnection8Line(key), key);
                        if (bakerlooCheck == "true") {
                            newLength += ((UndergroundData.getConnectionLength8(key)) / 2) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        } else {
                            newLength += UndergroundData.getConnectionLength8(key) + 1 + totalTimeForEachStation[Integer.parseInt(key)];
                        }
                    }
                }
                if (originalAdjacentLength > 0) {
                    if (newLength < originalAdjacentLength) {
                        Time(newLength, adjacentKey);
                        totalTimeForEachStation[adjacentKey] = newLength;
                        lastStationVisited[adjacentKey] = nextStation;
                        if (bakerlooCheck == "true") {
                            lineFromLastStation[adjacentKey] = "Bakerloo";
                        } else {
                            lineFromLastStation[adjacentKey] = UndergroundData.getConnection8Line(key);
                        }
                    }
                }

                if (originalAdjacentLength == 0) {
                    Time(newLength, adjacentKey);
                    totalTimeForEachStation[adjacentKey] = newLength;
                    lastStationVisited[adjacentKey] = nextStation;
                    if (bakerlooCheck == "true") {
                        lineFromLastStation[adjacentKey] = "Bakerloo";
                    } else {
                        lineFromLastStation[adjacentKey] = UndergroundData.getConnection8Line(key);
                    }
                }

            }
        }

        //This finds the closest station
        int smallestNumber = Integer.MAX_VALUE;
        int smallestKey = 0;
        for (int i = 0; i < trainStations.size(); i++) {
            if ((totalTimeForEachStation[i] > 0) && (permanentStations[i] == 0)) {
                if (smallestNumber > totalTimeForEachStation[i]) {
                    smallestNumber = totalTimeForEachStation[i];
                    smallestKey = i;
                }
            }
        }
        //the closest station is the next one to be reviewed
        nextStation = UndergroundData.getName(String.valueOf(smallestKey));
    }

    //builds the table and fills in colours
    //see top of code for website reference
    private JComponent createData(DefaultTableModel model) {
        JTable table = new JTable(model) {
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);

                //Colour row based on the tube line
                if (!isRowSelected(row)) {
                    c.setBackground(getBackground());
                    int modelRow = convertRowIndexToModel(row);
                    String type = (String) getModel().getValueAt(modelRow, 1);
                    if ("Bakerloo".equals(type)) {
                        c.setBackground(new Color(156, 93, 82));
                    }
                    if ("Circle".equals(type)) {
                        c.setBackground(Color.YELLOW);
                    }
                    if ("Central".equals(type)) {
                        c.setBackground(Color.RED);
                    }
                    if ("District".equals(type)) {
                        c.setBackground(Color.GREEN);
                    }
                    if ("Hammersmith & City".equals(type)) {
                        c.setBackground(Color.PINK);
                    }
                    if ("Jubilee".equals(type)) {
                        c.setBackground(Color.LIGHT_GRAY);
                    }
                    if ("Metropolitan".equals(type)) {
                        c.setBackground(Color.MAGENTA);
                    }
                    if ("Northern".equals(type)) {
                        c.setBackground(Color.DARK_GRAY);
                    }
                    if ("Piccadilly".equals(type)) {
                        c.setBackground(Color.BLUE);
                    }
                    if ("Victoria".equals(type)) {
                        c.setBackground(new Color(31, 190, 214));
                    }
                    if ("Waterloo & City".equals(type)) {
                        c.setBackground(Color.CYAN);
                    }
                }
                return c;
            }
        };

        table.setBackground(Color.white);
        table.setForeground(Color.BLACK);
        Font tableFont = new Font("", 1, 11);
        table.setFont(tableFont);
        table.setRowHeight(20);
        JScrollPane pane = new JScrollPane(table);
        return pane;
    }

    public static void main(String[] args) throws IOException {
        UndergroundDraft prg = new UndergroundDraft();
    }

}
