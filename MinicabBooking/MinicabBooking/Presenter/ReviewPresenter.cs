﻿using MinicabBooking.Model;
using MinicabBooking.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinicabBooking.Presenter
{
    public class ReviewPresenter
    {
        IReviewView _pView;
        IReviewModel _pModel;

        public ReviewPresenter(IReviewView PView, IReviewModel PModel) {
            _pView = PView;
            _pModel = PModel;
        }

        public void BindModalView()
        {
            List<String> ls = _pModel.StoreReview();
            _pView.DriverChoose = ls[0];
            _pView.StarChoose = ls[1];
            _pView.SubmitButton = ls[2];
        }

        public void Submit()
        {
            string name = _pView.UserName;
            string email = _pView.UserEmail;
            string phone = _pView.UserPhone;
            string comment = _pView.UserComment;
            string driver = _pView.Userdriver;
            string stars = _pView.UserStars;
            DBConnectivity.LeaveReview(name, email, phone, comment, stars, driver);

        }
    }
}