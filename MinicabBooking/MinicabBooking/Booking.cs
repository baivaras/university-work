﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinicabBooking
{
    public class Booking
    {

        private Client clientID;
        private Driver driverID;
        private DateTime time;
        private string serviceType;
        private int passengers;
        private string miniCab;
        private string pickUp;
        private string dropOff;
        private string confirmation;
        private string meetgreet;
        private string specInstructions;


        public Client ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        public Driver DriverID
        {
            get { return driverID; }
            set { driverID = value; }

        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }

        }

        public string ServiceType
        {
            get { return serviceType; }
            set { serviceType = value; }

        }
        public int Passengers
        {
            get { return passengers; }
            set { passengers = value; }

        }
        public string Minicab
        {
            get { return miniCab; }
            set { miniCab = value; }
        }
        public string PickUp
        {
            get { return pickUp; }
            set { pickUp = value; }

        }
        public string DropOff
        {
            get { return dropOff; }
            set { dropOff = value; }

        }

        public string Confirmation
        {
            get { return confirmation; }
            set { confirmation = value; }

        }
        public string Meetgreet
        {
            get { return meetgreet; }
            set { meetgreet = value; }

        }
        public string SpecInstructions
        {
            get { return specInstructions; }
            set { specInstructions = value; }

        }

        public Booking(Client cid, Driver did, DateTime ti, string st, int pa, string pu, string dof,string co,string mg ,string si, string mc )
        {
            ClientID = cid;
            DriverID = did;
            Time = ti;
            ServiceType = st;
            Passengers = pa;
            Minicab = mc;
            PickUp = pu;
            DropOff = dof;
            Confirmation = co;
            Meetgreet = mg;
            SpecInstructions = si;
            
        }
    }
}