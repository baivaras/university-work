﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Enquiry.aspx.cs" Inherits="MinicabBooking.Enquiry" %>--%>

<%@ Page Title="Submit Enquiry" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Enquiry.aspx.cs" Inherits="MinicabBooking.Enquiry" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form validate-form">
                    <span class="login100-form-title p-b-26">Enquiry
                    </span>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="nameText" runat="server" class="input100" ValidateRequestMode="Enabled"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Name"></span>
                    </div>
                    <asp:RequiredFieldValidator ID="rfvnaem" ErrorMessage="Enter Valid Name" ControlToValidate="nameText"
                        runat="server" />

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="emailText" runat="server" class="input100" ValidateRequestMode="Enabled"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="E-mail"></span>

                    </div>
                    <asp:RequiredFieldValidator ID="rfvemail" ErrorMessage="Enter Valid Email" ControlToValidate="emailText"
                        runat="server" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter the Vaild Email ID"
                        ControlToValidate="emailText" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="mobileText" runat="server" class="input100" ValidateRequestMode="Enabled"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Mobile number"></span>

                    </div>
                    <asp:RangeValidator ID="rangeph" ControlToValidate="mobileText" Text="Invalid Phone" Type="Double"
                        MinimumValue="1" MaximumValue="9999999999" runat="server" />

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="Subject" runat="server" class="input100" ValidateRequestMode="Enabled"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Subject"></span>

                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter Subject" ControlToValidate="Subject"
                        runat="server" />

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="enquiryText" runat="server" class="input100-review" ValidateRequestMode="Enabled" TextMode="MultiLine" Rows="7" MaxLength="1000"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Enquiry"></span>

                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter Enquiry" ControlToValidate="enquiryText"
                        runat="server" />


                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <asp:Button ID="enquiryButton" runat="server" Text="Submit Enquiry" CssClass="login100-form-btn buttonChange" OnClick="enquiryButton_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
