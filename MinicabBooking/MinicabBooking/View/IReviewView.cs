﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinicabBooking.View
{
    public interface IReviewView
    {
        string UserName { get; set; }
        string UserEmail { get; set; }
        string UserPhone { get; set; }
        string UserComment { get; set; }
        string Userdriver { get; set; }
        string UserStars { get; set; }

        string DriverChoose { get; set; }
        string StarChoose { get; set; }
        string SubmitButton { get; set; }
    }
}
