﻿using MinicabBooking.Model;
using MinicabBooking.Presenter;
using MinicabBooking.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MinicabBooking
{
    public partial class Review : System.Web.UI.Page, IReviewView
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            ReviewPresenter presenter = new ReviewPresenter(this, new ReviewModel());
            presenter.BindModalView();

            driversList.DataSource = DBConnectivity.LoadDriversList();
            driversList.DataTextField = "driverName";
            driversList.DataValueField = "driverID";
            driversList.DataBind();

        }

        #region IReviewView Implementation
        public string UserName {
            get
            {
                return nametxt.Text;
            }
            set
            {
                nametxt.Text = value;
            }
        }

        public string UserEmail
        {
            get
            {
                return emailtxt.Text;
            }
            set
            {
                emailtxt.Text = value;
            }
        }

        public string UserPhone
        {
            get
            {
                return phonetxt.Text;
            }
            set
            {
                phonetxt.Text = value;
            }
        }

        public string UserComment
        {
            get
            {
                return commenttxt.Text;
            }
            set
            {
                commenttxt.Text = value;
            }
        }

        public string Userdriver
        {
            get
            {
                return driversList.SelectedItem.Text;
            }
            set
            {
                driversList.SelectedItem.Text = value;
            }
        }

        public string UserStars
        {
            get
            {
                return starList.SelectedItem.Text;
            }
            set
            {
                starList.SelectedItem.Text = value;
            }
        }

        public string DriverChoose
        {
            get
            {
                return Label1.Text;
            }
            set
            {
                Label1.Text = value;
            }
        }

        public string StarChoose
        {
            get
            {
                return Label2.Text;
            }
            set
            {
                Label2.Text = value;
            }
        }

        public string SubmitButton
        {
            get
            {
                return submitReview.Text;
            }
            set
            {
                submitReview.Text = value;
            }
        }

        #endregion

        protected void submitReview_Click(object sender, EventArgs e)
        {


            ReviewPresenter presenter = new ReviewPresenter(this, new ReviewModel());
            presenter.Submit();
        }
    }
}