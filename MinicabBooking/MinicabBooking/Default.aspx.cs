﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MinicabBooking
{
    public partial class BookingMain : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {

        }

        protected void Submit1_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string a = "";
            string b = driversList.SelectedItem.Text;
            DateTime c = calJourney.SelectedDate;
            string d = textPick.Text;
            string pw = textDrop.Text;
            string meet = meetgreet.SelectedItem.Text;
            string spec = spectext.Text;
            int distance = int.Parse(DistanceBox.Text);
            int flatRate = 5;
            int sum = 0;
            int driverID = int.Parse(driversList.SelectedValue);


            if (RadioButtonList1.SelectedItem.Text == "Seven Seater")
            {
                flatRate = 10;
                a = RadioButtonList1.SelectedItem.Text;
            }
            else if (RadioButtonList1.SelectedItem.Text == "Five Seater")
            {
                a = RadioButtonList1.SelectedItem.Text;
            }

            sum = distance * flatRate;


            if (DistanceBox != null && driversList.SelectedValue != null && calJourney.SelectedDate != null && textPick.Text != "" && textDrop.Text != "" && meetgreet.SelectedValue != null && spectext.Text != "")
            {
                DBConnectivity.RecordBooking(driverID, c, d, pw, a, distance, meet, spec, sum);
                Response.Cookies["time"].Value = c.ToShortDateString();
                Response.Cookies["driverName"].Value = b;
                Response.Cookies["minicab"].Value = a;
                Response.Cookies["pickup"].Value = d;
                Response.Cookies["dropoff"].Value = pw;
                Response.Cookies["meetgreet"].Value = meet;
                Response.Cookies["specialinstructions"].Value = spec;
                Response.Cookies["distance"].Value = distance.ToString();
                Response.Cookies["price"].Value = sum.ToString();
                Response.Cookies["flatRate"].Value = flatRate.ToString();
                Response.Redirect("reciept.aspx");
            }
        }

        protected void radCab_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void FiveSeater_CheckedChanged(object sender, EventArgs e)
        {
       
        }

        protected void SevenSeater_CheckedChanged(object sender, EventArgs e)
        {
         
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedItem.Text == "Seven Seater")
            {
                driversList.DataSource = DBConnectivity.LoadDriversList7();
                driversList.DataTextField = "driverName";
                driversList.DataValueField = "driverID";
                driversList.DataBind();
            }
            else if (RadioButtonList1.SelectedItem.Text == "Five Seater")
            {
                driversList.DataSource = DBConnectivity.LoadDriversList5();
                driversList.DataTextField = "driverName";
                driversList.DataValueField = "driverID";
                driversList.DataBind();
            }
        }
    }
}
    