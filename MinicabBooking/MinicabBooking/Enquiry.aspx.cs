﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MinicabBooking
{
    public partial class Enquiry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void enquiryButton_Click(object sender, EventArgs e)
        {
            string name = nameText.Text;
            string email = emailText.Text;
            string mobile = mobileText.Text;
            string subject = Subject.Text;
            string enquiry = enquiryText.Text;

            DBConnectivity.Enquiry(name, email, mobile, subject, enquiry);
        }
    }
}