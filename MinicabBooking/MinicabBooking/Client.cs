﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinicabBooking
{
    public class Client
    {
        private int clientID;
        private string clientName;
        private string clientSurname;
        private string clientEmail;
        private string clientPhone;
        private int clientCardDetails;

        public int ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }

        }
        public string ClientSurname
        {
            get { return clientSurname; }
            set { clientSurname = value; }

        }
        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }

        }
        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }

        }
        public int ClientCardDetails
        {
            get { return clientCardDetails; }
            set { clientCardDetails = value; }

        }

        public Client(int id, string n, string s, string e, string p, int c)
        {
            ClientID = id;
            ClientName = n;
            ClientSurname = s;
            ClientEmail = e;
            ClientPhone = p;
            ClientCardDetails = c;
        }
    }
}