﻿<%@ Page Title="Book Service" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MinicabBooking.BookingMain" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form validate-form">
                    <span class="login100-form-title p-b-26">Enter your booking details
                    </span>

                    <fieldset>
                        <legend>Which minicab do you require?</legend>

                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Text="Seven Seater" Value="Seven Seater">Seven Seater</asp:ListItem>
                            <asp:ListItem Text="Five Seater" Value="Five Seater" Selected="False">Five Seater</asp:ListItem>
                        </asp:RadioButtonList>


                       <%-- <asp:RadioButton ID="SevenSeater" Text="Seven Seater" runat="server" AutoPostBack="True" OnCheckedChanged="SevenSeater_CheckedChanged" />
                        <br />
                        <asp:RadioButton ID="FiveSeater" Text="Five Seater" runat="server" OnCheckedChanged="FiveSeater_CheckedChanged" AutoPostBack="True" />--%>

                    </fieldset>
                    <fieldset>

                        <span>Booking Details</span>
                        <br />
                        <br />
                        Driver
                        <asp:DropDownList ID="driversList" runat="server" CssClass="form-control">
                        </asp:DropDownList>

                        
                    </fieldset>


                    <p>
                        <label>
                            Pickup Date/Time
                        </label>
                    </p>
                    <p>
                        <asp:Calendar ID="calJourney" runat="server" CssClass="date" >
                            
                        </asp:Calendar>
                    </p>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="textPick" runat="server" class="input100"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Pickup place"></span>
                    </div>


                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="textDrop" runat="server" class="input100"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Dropoff place"></span>
                    </div>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="DistanceBox" runat="server" class="input100"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Approximate distance"></span>
                    </div>

                    <br />

                    <strong>Would you like meet & greet service?</strong>

                    <asp:RadioButtonList ID="meetgreet" runat="server" CssClass="radio">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="spectext" runat="server" class="input100"></asp:TextBox>
                        <span class="focus-input100" data-placeholder="Special instructions"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                        <asp:Button ID="Button1" runat="server" Text="Make a booking" OnClick="Button1_Click" CssClass="login100-form-btn buttonChange" />
                            </div>
                        </div>
                    

                </div>
                <%--<div class="tab-pane" id="tab2">
                <fieldset>
                    <legend>Select Airport</legend>
                    <select name="passengers">
                        <option value="Stansted">London Stansted</option>
                        <option value="Luton">London Luton</option>
                        <option value="Gatwick">London Gatwick</option>
                        <option value="Heathrow">Heathrow</option>
                    </select>
                    <p>
                        <label>
                            Arrival Time
                        </label>
                    </p>
                    <p>
                        <asp:Calendar ID="Calendar3" runat="server"></asp:Calendar>
                    </p>
                </fieldset>

                <fieldset>
                    <legend>Booking Details</legend>

                    <label>
                        Number of passengers
     <select name="passengers">
         <option value="1">1</option>
         <option value="2">2</option>
         <option value="3">3</option>
         <option value="4">4</option>
         <option value="5">5</option>
         <option value="6">6</option>

     </select>
                    </label>
                </fieldset>

                <p>
                    <label>
                        Hotel
 <select name="hotels">
     <option value="hilton">Hilton</option>
     <option value="soho">Soho Hotel</option>
     <option value="premier">Premier Inn</option>
     <option value="lodge">Travel Lodge</option>
     <option value="pond">Pond Hotel</option>
     <option value="claridge">Claridge</option>
 </select>
                    </label>
                </p>


                <strong>How would you like to receive confirmation?</strong>
                <p>
                    <input type="checkbox" name="extras" value="baby">
                    Message </p>
                <p>
                    <input type="checkbox" name="extras" value="wheelchair">
                    Email </p>
                <p>
                    <input type="checkbox" name="extras" value="tip">
                    Call Back </p>
                <p>

                    <strong>Would you like meet & greet service?</strong>
                    <div class="radio">
                        <label>
                            <input type="radio" name="meetgreet">Yes</label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="meetgreet">No</label>
                    </div>

                </p>
                <p>
                    <label>
                        Special Instructions
                        <textarea name="comments" maxlength="500"></textarea>
                    </label>
                </p>

                <p>
                    <button>Submit Booking</button></p>
            </div>
            <div class="tab-pane" id="tab3">
                <fieldset>
                    <legend>Select Hotel</legend>
                    <select name="hotels">
                        <option value="hilton">Hilton</option>
                        <option value="soho">Soho Hotel</option>
                        <option value="premier">Premier Inn</option>
                        <option value="lodge">Travel Lodge</option>
                        <option value="pond">Pond Hotel</option>
                        <option value="claridge">Claridge</option>
                    </select>
                    <p>
                        <label>
                            Departure Time
                        </label>
                    </p>
                    <p>
                        <asp:Calendar ID="Calendar2" runat="server"></asp:Calendar>
                    </p>
                </fieldset>

                <fieldset>
                    <legend>Booking Details</legend>

                    <label>
                        Number of passengers
     <select name="passengers">
         <option value="1">1</option>
         <option value="2">2</option>
         <option value="3">3</option>
         <option value="4">4</option>
         <option value="5">5</option>
         <option value="6">6</option>

     </select>
                    </label>
                </fieldset>

                <p>
                    <label>
                        Airport
                        <select name="passengers">
                            <option value="Stansted">London Stansted</option>
                            <option value="Luton">London Luton</option>
                            <option value="Gatwick">London Gatwick</option>
                            <option value="Heathrow">Heathrow</option>
                        </select>
                    </label>
                </p>



                <strong>How would you like to receive confirmation?</strong>
                <p>
                    <input type="checkbox" name="extras" value="baby">
                    Message </p>
                <p>
                    <input type="checkbox" name="extras" value="wheelchair">
                    Email </p>
                <p>
                    <input type="checkbox" name="extras" value="tip">
                    Call Back </p>


                <strong>Would you like meet & greet service?
                    <div class="radio">
                        <label>
                            <input type="radio" name="meetgreet">Yes</label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="meetgreet">No</label>
                    </div>
                </strong>

                <p>
                    <label>
                        Special Instructions
                        <textarea name="comments" maxlength="500"></textarea>
                    </label>
                </p>

                <p>
                    <button>Submit Booking</button></p>
            </div>
            <div class="tab-pane" id="tab4">
                <fieldset>
                    <legend>Which minicab do you require?</legend>
                    <p>
                        <label>
                            <input type="radio" name="taxi" required value="Fiveseater">
                            Five seater </label>
                    </p>
                    <p>
                        <label>
                            <input type="radio" name="taxi" required value="Sevenseater">
                            Seven seater </label>
                    </p>
                </fieldset>
                <fieldset>

                    <legend>Booking Details</legend>
                    <select name="passengers">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>

                    </select>
                    Passengers
                </fieldset>


                <p>
                    <label>
                        How long you need a car?
                    </label>
                </p>


                <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
                <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
                <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />



                <input type="text" class="daterange" />


                <script type="text/javascript">
                    $('.daterange').daterangepicker();
                </script>





                <p>
                    <label>
                        Pickup Place
                        <input type="text" name="Pickup_place" required list="pickups">
                    </label>
                </p>

                <p>
                    <label>
                        Dropoff Place
                        <input type="text" name="dropoff_place" required list="destinations">
                    </label>
                </p>

                <strong>How would you like to receive confirmation?</strong>
                <p>
                    <input type="checkbox" name="extras" value="baby">
                    Message </p>
                <p>
                    <input type="checkbox" name="extras" value="wheelchair">
                    Email </p>
                <p>
                    <input type="checkbox" name="extras" value="tip">
                    Call Back </p>

                <strong>Would you like meet & greet service?</strong>
                <div class="radio">
                    <label>
                        <input type="radio" name="meetgreet">Yes</label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="meetgreet">No</label>
                </div>



                <p>
                    <label>
                        Special Instructions
                        <textarea name="comments" maxlength="500"></textarea>
                    </label>
                </p>

                <p>
                    <button>Submit Booking</button></p>
            </div>--%>
            </div>
        </div>

    </div>


</asp:Content>

