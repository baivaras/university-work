﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Review.aspx.cs" Inherits="MinicabBooking.Review" %>--%>

<%@ Page Title="Book Service" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Review.aspx.cs" Inherits="MinicabBooking.Review" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form validate-form">
                    <span class="login100-form-title p-b-26">Leave a review
                    </span>

                    <!-- Body Content Starts -->
                    <div id="formBody">

                        <div class="wrap-input100 validate-input">
                            <asp:TextBox ID="nametxt" runat="server" CssClass="input100" />
                            <span class="focus-input100" data-placeholder="Name">
                                </span>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvnaem" ErrorMessage="*" ControlToValidate="nametxt"
                                    runat="server" />

                        <div class="wrap-input100 validate-input">
                            <asp:TextBox ID="emailtxt" runat="server" CssClass="input100" />
                            <span class="focus-input100" data-placeholder="E-mail">
                                </span>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvemail" ErrorMessage="*" ControlToValidate="emailtxt"
                                    runat="server" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter the Vaild Email ID"
                                    ControlToValidate="emailtxt" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />

                        <div class="wrap-input100 validate-input">
                            <asp:TextBox ID="phonetxt" runat="server" CssClass="input100" />
                            <span class="focus-input100" data-placeholder="Phone number">
                                </span>
                        </div>
                        <asp:RangeValidator ID="rangeph" ControlToValidate="phonetxt" Text="Invalid Phone" Type="Double"
                                    MinimumValue="1" MaximumValue="9999999999" runat="server" />

                        <div class="col-xs-12 padding-class">
                        <strong>
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label></strong>
                        <asp:DropDownList ID="driversList" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                            </div>

                        <div class="col-xs-12 padding-class">
                       <strong> <asp:Label ID="Label2" runat="server" Text=""></asp:Label></strong>
                        <asp:DropDownList ID="starList" runat="server" CssClass="form-control">
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="0">0</asp:ListItem>
                        </asp:DropDownList>
                            </div>
                        <asp:RangeValidator ID="RangeValidator1" ControlToValidate="starList" Text="Invalid Rating" Type="Integer"
                            MinimumValue="1" MaximumValue="5" runat="server" />

                        <div class="wrap-input100 validate-input">
                            <asp:TextBox ID="commenttxt" runat="server" MaxLength="1000" TextMode="MultiLine" CssClass="input100-review" />
                            <span class="focus-input100" data-placeholder="Comment (Max 1000char)"></span>
                                
                        </div>
                        <asp:RequiredFieldValidator ID="rfvcomment" ErrorMessage="Comment is required" ControlToValidate="commenttxt"
                                    runat="server" />

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <asp:Button ID="submitReview" runat="server" Text="" OnClick="submitReview_Click" CssClass="login100-form-btn buttonChange" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

