﻿using MinicabBooking.Model;
using MinicabBooking.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MinicabBooking
{
    public class DBConnectivity
    {
        //IReviewView _pView;
        //IReviewModel _pModel;
        private static SqlConnection GetConnection()
        {
            string connString;
            connString = @"Data Source=SQL-SERVER;Initial Catalog=ab0264b;Persist Security Info=True;User ID=ab0264b;Password=B@ivaras1";
            return new SqlConnection(connString);
        }

        public static object ExecuteScaler(string qry)
        {
            object ret = null;
            using (SqlConnection con = GetConnection())
            {
                SqlCommand cmd = new SqlCommand(qry, con);
                con.Open();
                ret = (cmd.ExecuteScalar());
                con.Close();
            }
            return ret;
        }

        public static int ExecuteNonQuery(string qry)
        {
            int ret = 0;
            using (SqlConnection con = GetConnection())
            {
                SqlCommand cmd = new SqlCommand(qry, con);
                con.Open();
                ret = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
            }
            return ret;

        }

        //method that saves a user in the db
        public static void SaveUser(string uFN, string uSN, string uE, string uCM, string uN, string uCS, string uCC)
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "INSERT INTO Driver(driverName, driverSurname, driverEmail, driverPhone, driverCarMake, driverCarSeats, driverCarColor) VALUES ('" + uFN + "' , '" + uSN + "' , '" + uE + "' , '" + uN + "' ,  '" + uCM + "', '" + uCS + "', '" + uCC + "'  )";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }



        }


        public static List<Driver> LoadDriversList()
        {
            List<Driver> drivers = new List<Driver>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT * FROM Driver";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Driver driv = new Driver(int.Parse(myReader["driverID"].ToString()), myReader["driverName"].ToString());
                    drivers.Add(driv);
                }
                return drivers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static void LeaveReview(string name, string email, string phone, string comment, string stars, string driver)
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "INSERT INTO Review(name, email, phone, comment, stars, driver) VALUES ('" + name + "' , '" + email + "' , '" + phone + "' , '" + comment + "' , '" + stars + "' , '" + driver + "' )";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static void Enquiry(string name, string email, string mobile, string subject, string enquiry)
        {
            SqlConnection myConnection = GetConnection();
            string myQuery = "INSERT INTO Enquiry(name, email, mobile, subject, enquiry) VALUES ('" + name + "' , '" + email + "' , '" + mobile + "' , '" + subject + "' , '" + enquiry + "' )";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }


        public static List<Driver> LoadDriversList7()
        {
            List<Driver> drivers = new List<Driver>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT * FROM Driver WHERE driverCarSeats = '7'";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Driver driv = new Driver(int.Parse(myReader["driverID"].ToString()), myReader["driverName"].ToString());
                    drivers.Add(driv);
                }
                return drivers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static List<Driver> LoadDriversList5()
        {
            List<Driver> drivers = new List<Driver>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT * FROM Driver WHERE driverCarSeats = '5'";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Driver driv = new Driver(int.Parse(myReader["driverID"].ToString()), myReader["driverName"].ToString());
                    drivers.Add(driv);
                }
                return drivers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static List<Driver> LoadDriversListByseats(int seat)
        {
            List<Driver> drivers = new List<Driver>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT * FROM Driver WHERE driverCarSeats =" + seat;
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Driver driv = new Driver(int.Parse(myReader["driverID"].ToString()), myReader["driverName"].ToString());
                    drivers.Add(driv);
                }
                return drivers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }

        //method to record booking
        public static void RecordBooking(int driverID, DateTime time, string pickup, string dropof, string minicab, int distance, string meet, string spec, int price)
        {
            // List<String> client = new List<String>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "INSERT INTO Booking (driverID, time, minicab, pickup ,dropoff, meetgreet, specInstructions, distance, price) VALUES ('" + driverID + "' , '" + time + "' , '" + minicab + "', '" + pickup + "','" + dropof + "', '" + meet + "','" + spec + "','" + distance + "','" + price + "')";
            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();



            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
            }
            finally
            {
                myConnection.Close();
            }
        }


        public static List<String> LoadDrivers(int seats)
        {
            List<String> drivers = new List<String>();
            SqlConnection myConnection = GetConnection();

            string myQuery = "SELECT  driverName  FROM Driver WHERE Driver.driverID =" + seats;

            SqlCommand myCommand = new SqlCommand(myQuery, myConnection);

            try
            {
                myConnection.Open();
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {


                    drivers.Add( myReader["Name"].ToString());

                }
                return drivers;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in DBHandler", ex);
                return null;
            }
            finally
            {
                myConnection.Close();
            }
        }
    }
}