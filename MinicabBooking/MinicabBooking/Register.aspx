﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Register.aspx.cs" Inherits="MinicabBooking.Register" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="limiter">
		<div class="container-login100">

            <div class=" wrap-login100">
	
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						Driver Register
					</span>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="FirstName" runat="server" CssClass="input100" name="firstname"></asp:TextBox>
						<span class="focus-input100" data-placeholder="First Name"></span>
					</div>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="SecondName" runat="server" CssClass="input100" name="secondname"></asp:TextBox>
						<span class="focus-input100" data-placeholder="Second Name"></span>
					</div>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="TelephoneNumber" runat="server" CssClass="input100" name="telephonenumber"></asp:TextBox>
						<span class="focus-input100" data-placeholder="Telephone Number"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <asp:TextBox ID="Email" runat="server" CssClass="input100" name="email"></asp:TextBox>
						<span class="focus-input100" data-placeholder="E-mail"></span>
					</div>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="CarMake" runat="server" CssClass="input100" name="carmake"></asp:TextBox>
						<span class="focus-input100" data-placeholder="Car Make"></span>
					</div>

                    <div class="wrap-input100 validate-input">
                        <asp:TextBox ID="CarColor" runat="server" CssClass="input100" name="carcolor"></asp:TextBox>
						<span class="focus-input100" data-placeholder="Car Colour"></span>
					</div>

                    <div class="wrap-input100 validate-input">
                        Choose car seats
                        <asp:RadioButtonList ID="Seater" runat="server">
                            
                            <asp:ListItem Value="7" Text="7">7</asp:ListItem>
                            <asp:ListItem Text="5" Value="5">5</asp:ListItem>
                        </asp:RadioButtonList>
                        </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
                            <asp:Button ID="Button1" runat="server" CssClass="login100-form-btn buttonChange" Text="Register" OnClick="Register_Click" />
						</div>
					</div>

                    <div id="Div1" runat="server" visible="false" class="alert alert-danger">
                        <strong>Error!</strong>
                        <asp:Label ID="Label1" runat="server" />
                    </div>

				</div>
			    </div>
		</div>
	</div>
</asp:Content>
