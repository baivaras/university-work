﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinicabBooking
{
    public class Driver
    {
        private int driverID;
        private string driverName;


        public int DriverID
        {
            get { return driverID; }
            set { driverID = value; }
        }

        public string DriverName
        {
            get { return driverName; }
            set { driverName = value; }

        }


        public Driver(int id, string n)
        {
            DriverID = id;
            DriverName = n;
        }
    }
}