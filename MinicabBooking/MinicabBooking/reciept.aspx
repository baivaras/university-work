﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="reciept.aspx.cs" Inherits="MinicabBooking.reciept" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div id="form1" runat="server">
        <div id="PrintArea">
            <table>
                <tr>
                    <td>
                        <table class="table">
                            <tr>
                                <td >
                                    <asp:Image ID="Image1" runat="server" Height="60px" ImageUrl="~/Images/logo.gif" Width="105px" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td>Booking Number </td>
                                <td class="arial18">: </td>
                                <td class="redText"><span><strong>
                                    <asp:Literal ID="idNumber" runat="server" />
                                    </strong></span></td>
                                <td>Driver Name </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="driverName" runat="server" />
                                    </strong></span></td>
                            </tr>
                            <tr>
                                <td>Name </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="customerName" runat="server" />
                                    </strong></span></td>
                                <td>Phone Number </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="phoneNumber" runat="server" />
                                    </strong></span></td>

                            </tr>

                            <tr>
                                
                                <td>Destination </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="dropOff" runat="server" />
                                    </strong></span></td>
                                <td>Special instructions</td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="special" runat="server" />
                                    </strong></span></td>
                            </tr>
                            <tr>
                                <td>Booking Date </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="bookingDate" runat="server" />
                                    </strong></span></td> 
                                <td>Meet&Greet service </td>
                                <td class="arial18">: </td>
                                <td class="redText"><span><strong>
                                    <asp:Literal ID="meetGreet" runat="server" />
                                    </strong></span></td>
                            </tr>
                            <tr>
                                <td>Seat Numbers </td>
                                <td class="arial18">: </td>
                                <td><span><strong>
                                    <asp:Literal ID="seatNumber" runat="server" />
                                    </strong></span></td>
                            </tr>
                            <tr>
                                <td>Flat Rate </td>
                                <td class="arial18">: </td>
                                <td class="redText"><span><strong>
                                    <asp:Literal ID="flatRate" runat="server" /> £
                                    </strong></span></td>
                                
                            </tr>
                            <tr>
                                <td>Distance </td>
                                <td class="arial18">: </td>
                                <td class="redText"><span><strong>
                                    <asp:Literal ID="distance" runat="server" /> miles
                                    </strong></span></td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Total Price </td>
                                <td class="arial18">: </td>
                                <td class="redText"><span><strong>
                                    <asp:Literal ID="totalPrice" runat="server" />£
                                    </strong></span></td>
                                <td >&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </asp:Content>

