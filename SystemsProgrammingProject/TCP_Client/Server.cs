﻿/*	File			TCP_Server_Form.cs
	Purpose			TCP Server example for demonstration purposes
	Author			Richard Anthony	(R.J.Anthony@gre.ac.uk)
                    structure modifications made by James Hawthorne (J.Hawthorne@gre.ac.uk)
	Date			December 2011
	
	Special notes:
		This code has been specially prepared to demonstrate how to create an application using TCP over IP.
		Students following the "Systems Programming" course may use this
		code as a starting point for the development of their coursework.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;

namespace PSRGame
{
    /* This structure is fully compatible with the c++ equivelent and is used by default to exchange messages
     * between c# and c#, c++ and c# in any direection.
     * 
     * Marshaling and demarshaling is the process of converting objects into a stream and vice versa
     * (see Serialize() and Deserialize() methods later in this code)
     * This is important because only streams can be sent over a network and should be converted back to an
     * object once the receiver gets it in byte[] form
     * 
     * IMPORTANT: Whatever structure is used, they must be identical on both ends of the network
     */
    [StructLayout(LayoutKind.Sequential)]
    public struct Message_PDU
    {
        // when the structure is marshaled we can force the size and type of variables to match that on the c++ side with the following attribute.
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 101)]
        public string action;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 101)]
        public string index;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 101)]
        public string playerChoice;

        public int iMessageNumber; //not used in c# version but is needed for compatibility with the c++ code
    }

    public partial class Server : Form
    {
        private const int m_iMaxConnections = 100;

        struct Connection_Struct    // Define a structure to hold details about a single connection
        {
            public Socket ClientSpecific_Socket;
            public bool bInUse;
        };

        Socket m_ListenSocket;
        Connection_Struct[] m_Connection_Array = new Connection_Struct[m_iMaxConnections]; // Define an array to hold a number of connections

        System.Net.IPEndPoint m_LocalIPEndPoint;
        static int m_iNumberOfConnectedClients;
        private static System.Windows.Forms.Timer m_CommunicationActivity_Timer;

        public Server()
        {
            InitializeComponent();
            Initialise_ConnectionArray();
            m_CommunicationActivity_Timer = new System.Windows.Forms.Timer(); // Check for communication activity on Non-Blocking sockets every 200ms
            m_CommunicationActivity_Timer.Tick += new EventHandler(OnTimedEvent_PeriodicCommunicationActivityCheck); // Set event handler method for timer
            m_CommunicationActivity_Timer.Interval = 100;  // Timer interval is 1/10 second
            m_CommunicationActivity_Timer.Enabled = false;
            string szLocalIPAddress = GetLocalIPAddress_AsString(); // Get local IP address as a default value
            IP_Address_textBox.Text = szLocalIPAddress;             // Place local IP address in IP address field
            ReceivePort_textBox.Text = "8000";  // Default port number
            m_iNumberOfConnectedClients = 0;
            NumberOfClients_textBox.Text = System.Convert.ToString(m_iNumberOfConnectedClients);
           
            try
            {   // Create the Listen socket, for TCP use
                m_ListenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                m_ListenSocket.Blocking = false;
            }
            catch (SocketException se)
            {   // If an exception occurs, display an error message
                MessageBox.Show(se.Message);
            }
        }

        private void Initialise_ConnectionArray()
        {
            int iIndex;
            for (iIndex = 0; iIndex < m_iMaxConnections; iIndex++)
            {
                m_Connection_Array[iIndex].bInUse = false;
            }
        }

        private int GetnextAvailable_ConnectionArray_Entry()
        {
            int iIndex;
            for (iIndex = 0; iIndex < m_iMaxConnections; iIndex++)
            {
                if (false == m_Connection_Array[iIndex].bInUse)
                {
                    return iIndex;  // Return the index value of the first not-in-use entry found
                }
            }
            return -1;      // Signal that there were no available entries
        }

        private void Connect_button_Click(object sender, EventArgs e)
        {    // Bind to the selected port and start listening / receiving
            try
            {
                // Get the Port number from the appropriate text box
                String szPort = ReceivePort_textBox.Text;
                int iPort = System.Convert.ToInt16(szPort, 10);
                // Create an Endpoint that will cause the listening activity to apply to all the local node's interfaces
                m_LocalIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, iPort);
                // Bind to the local IP Address and selected port
                m_ListenSocket.Bind(m_LocalIPEndPoint);
                Connect_button.Enabled = false;
                Connect_button.Text = "Connected";
               // Listen_button.Enabled = true;
                // Prevent any further changes to the port number
                ReceivePort_textBox.ReadOnly = true;
                m_ListenSocket.Listen(2);
                m_CommunicationActivity_Timer.Start();  // Start the timer to perform periodic checking for connection requests   
            }

            catch (SocketException se) // Catch any errors
            {   // If an exception occurs, display an error message
                MessageBox.Show(se.Message);
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(se.Message);
                m_ListenSocket.Send(byData, SocketFlags.None);
            }
        }

        public string GetLocalIPAddress_AsString()
        {
            string szHost = Dns.GetHostName();
            string szLocalIPaddress = "127.0.0.1";  // Default is local loopback address
            IPHostEntry IPHost = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IP in IPHost.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork) // Match only the IPv4 address
                {
                    szLocalIPaddress = IP.ToString();
                    break;
                }
            }
            return szLocalIPaddress;
        } 

        private void About_button_Click(object sender, EventArgs e)
        {
            About_Form MyAbout_Form = new About_Form();
            MyAbout_Form.Show();
        }

        private void Done_button_Click(object sender, EventArgs e)
        {
            Close_And_Quit();
        }

        private void Close_And_Quit()
        {   // Close the sockets and exit the application
            try
            {
                m_ListenSocket.Close();
            }
            catch
            {
            }
            try
            {
                int iIndex;
                for (iIndex = 0; iIndex < m_iMaxConnections; iIndex++)
                {
                    m_Connection_Array[iIndex].ClientSpecific_Socket.Shutdown(SocketShutdown.Both);
                    m_Connection_Array[iIndex].ClientSpecific_Socket.Close();
                }
            }
            catch
            {
            }
            try
            {
                Close();
            }
            catch
            {
            }
        }

        public void ByteSend(Message_PDU myPDU)
        {
            int i;

            for (i = 0; i < m_iMaxConnections; i++)
            {
                byte[] byData = Serialize(myPDU); //convert structure into a byte[]
                m_Connection_Array[i].ClientSpecific_Socket.Send(byData, SocketFlags.None);
            }
        }

        private void OnTimedEvent_PeriodicCommunicationActivityCheck(Object myObject, EventArgs myEventArgs)
        {   // Periodic check whether a connection request is pending or a message has been received on a connected socket     

            // First, check for pending connection requests
            int iIndex;
            iIndex = GetnextAvailable_ConnectionArray_Entry(); // Find an available array entry for next connection request
            if (-1 != iIndex)
            {   // Only continue with Accept if there is an array entry available to hold the details

                try
                {
                    m_Connection_Array[iIndex].ClientSpecific_Socket = m_ListenSocket.Accept();  // Accept a connection (if pending) and assign a new socket to it (AcceptSocket)
                    // Will 'catch' if NO connection was pending, so statements below only occur when a connection WAS pending
                    m_Connection_Array[iIndex].bInUse = true;
                    m_Connection_Array[iIndex].ClientSpecific_Socket.Blocking = false;           // Make the new socket operate in non-blocking mode
                    m_iNumberOfConnectedClients++;
                    NumberOfClients_textBox.Text = System.Convert.ToString(m_iNumberOfConnectedClients);
                    Status_textBox.Text = "A new client connected";

                    SendUpdateMesageToAllConnectedclients();
                }
                catch (SocketException se) // Handle socket-related exception
                {   // If an exception occurs, display an error message
                    if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                    {
                        CloseConnection(iIndex);
                    }
                    else if (10035 != se.ErrorCode)
                    {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                        MessageBox.Show(se.Message);
                    }
                }
                catch // Silently handle any other exception
                {
                }
            }

           

            // Second, check for received messages on each connected socket
            for (iIndex = 0; iIndex < m_iMaxConnections; iIndex++)
            {
                if (true == m_Connection_Array[iIndex].bInUse)
                {
                    try
                    {
                        EndPoint localEndPoint = (EndPoint)m_LocalIPEndPoint;
                        byte[] ReceiveBuffer = new byte[1024];
                        int iReceiveByteCount;
                        iReceiveByteCount = m_Connection_Array[iIndex].ClientSpecific_Socket.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                        byte[] data = new byte[iReceiveByteCount];
                        Array.Copy(ReceiveBuffer, data, iReceiveByteCount); //make sure byte[] is the same length as the recieved byte[]

                        if (0 < iReceiveByteCount)
                        {
                            try
                            {
                                Message_PDU myPDU = DeSerialize(data); //convert byte[] to structure so it can be read

                                switch (myPDU.action)
                                {
                                    case "Disconnect":
                                        CloseConnection(iIndex);
                                        break;
                                    case "!!":
                                        ByteSend(myPDU);
                                        break;
                                    case "--":
                                        ByteSend(myPDU);
                                        break;
                                    case "**":
                                        ByteSend(myPDU);
                                        break;
                                    case "rps":
                                        ByteSend(myPDU);
                                        break;
                                    default:
                                        Message_textBox.Text = myPDU.action; //display received message in edit box
                                        break;

                                }
                            }
                            catch (Exception e) { System.Console.WriteLine(e.Message); }
                        }
                    }
                    catch (SocketException se) // Handle socket-related exception
                    {   // If an exception occurs, display an error message
                        if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                        {
                            CloseConnection(iIndex);
                        }
                        else if (10035 != se.ErrorCode)
                        {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                            MessageBox.Show(se.Message);
                        }
                    }
                    catch // Silently handle any other exception
                    {
                    }
                }
            }
        }

        //converts a byte[] to a Message_PDU structure
        public Message_PDU DeSerialize(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Message_PDU structure = (Message_PDU)Marshal.PtrToStructure(ip, typeof(Message_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        private void SendUpdateMesageToAllConnectedclients()
        {   // Send message to each connected client informing of the total number of connected clients
            int iIndex;
            for (iIndex = 0; iIndex < m_iMaxConnections; iIndex++)
            {
                if (true == m_Connection_Array[iIndex].bInUse)
                {
                    string updateM;
                    if (1 == m_iNumberOfConnectedClients)
                    {
                        updateM = string.Format("There is now {0} client connected", m_iNumberOfConnectedClients);
                    }
                    else
                    {
                        updateM = string.Format("There are now {0} clients connected", m_iNumberOfConnectedClients);
                    }
                    byte[] SendMessage = System.Text.Encoding.ASCII.GetBytes(updateM); //convert Message_PDU into a byte[] for sending
                    m_Connection_Array[iIndex].ClientSpecific_Socket.Send(SendMessage, SocketFlags.None);
                }
            }
        }

        private void CloseConnection(int iIndex)
        {
            try
            {
                m_Connection_Array[iIndex].bInUse = false;
                m_Connection_Array[iIndex].ClientSpecific_Socket.Shutdown(SocketShutdown.Both);
                m_Connection_Array[iIndex].ClientSpecific_Socket.Close();
                m_iNumberOfConnectedClients--;
                NumberOfClients_textBox.Text = System.Convert.ToString(m_iNumberOfConnectedClients);
                Status_textBox.Text = "A Connection was closed";
                SendUpdateMesageToAllConnectedclients();
            }
            catch // Silently handle any exceptions
            {
            }
        }

        //converts any object into a byte[]
        private byte[] Serialize(Object myObject)
        {
            int size = Marshal.SizeOf(myObject);
            IntPtr ip = Marshal.AllocHGlobal(size); //allocate unmanaged memory equivelent to the size of the object
            Marshal.StructureToPtr(myObject, ip, false); //marshal the object into the allocated memory
            byte[] byteArray = new byte[size];
            Marshal.Copy(ip, byteArray, 0, size); //place the contents of memory into a byte[]
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return byteArray;
        }
    }
}
