﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PSRGame
{
    public partial class MainClient : Form
    {
        public MainClient()
        {
            InitializeComponent();
        }

        private void MainClient_Load(object sender, EventArgs e)
        {

        }

        private void server_button_Click(object sender, EventArgs e)
        {
            InitializeComponent();
            Server server = new Server();
            server.Show();
        }

        private void client_button_Click(object sender, EventArgs e)
        {
            InitializeComponent();
            Client client = new Client();
            client.Show();
        }
    }
}
