﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;

namespace PSRGame
{
    /* This structure is fully compatible with the c++ equivelent and is used by default to exchange messages
     * between c# and c#, c++ and c# in any direection.
     * 
     * Marshaling and demarshaling is the process of converting objects into a stream and vice versa
     * (see Serialize() and Deserialize() methods later in this code)
     * This is important because only streams can be sent over a network and should be converted back to an
     * object once the receiver gets it in byte[] form
     * 
     * IMPORTANT: Whatever structure is used, they must be identical on both ends of the network
     */
    

    public partial class Client : Form
    {
        Socket m_ClientSocket;
        System.Net.IPEndPoint m_remoteEndPoint;
        private static System.Windows.Forms.Timer m_CommunicationActivity_Timer;
        private static System.Timers.Timer delay;

        //private Rectangle[] boardColumns;
        Message_PDU myPDU = new Message_PDU();

        public Client()
        {
            InitializeComponent();
            m_CommunicationActivity_Timer = new System.Windows.Forms.Timer(); // Check for communication activity on Non-Blocking sockets every 200ms
            m_CommunicationActivity_Timer.Tick += new EventHandler(OnTimedEvent_PeriodicCommunicationActivityCheck); // Set event handler method for timer
            m_CommunicationActivity_Timer.Interval = 100;  // Timer interval is 1/10 second
            //m_CommunicationActivity_Timer.Enabled = false;

            CloseConnection_button.Enabled = false;
            CloseConnection_button.Text = "Close Connection";
            string szLocalIPAddress = GetLocalIPAddress_AsString(); // Get local IP address as a default value
            IP_Address_textBox.Text = szLocalIPAddress;             // Place local IP address in IP address field
            SendPort_textBox.Text = "8000"; // Default port number
           // Send_button.Enabled = false;
           // this.

            
        }

        private string GetLocalIPAddress_AsString()
        {
            string szHost = Dns.GetHostName();
            string szLocalIPaddress = "127.0.0.1";  // Default is local loopback address
            IPHostEntry IPHost = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IP in IPHost.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork) // Match only the IPv4 address
                {
                    szLocalIPaddress = IP.ToString();
                    break;
                }
            } return szLocalIPaddress;
        }

        private void About_button_Click(object sender, EventArgs e)
        {
            About_Form MyAbout_Form = new About_Form();
            MyAbout_Form.Show();
        }

      

        //converts any object into a byte[]
        private byte[] Serialize(Object myObject)
        {
            int size = Marshal.SizeOf(myObject);
            IntPtr ip = Marshal.AllocHGlobal(size); //allocate unmanaged memory equivelent to the size of the object
            Marshal.StructureToPtr(myObject, ip, false); //marshal the object into the allocated memory
            byte[] byteArray = new byte[size];
            Marshal.Copy(ip, byteArray, 0, size); //place the contents of memory into a byte[]
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return byteArray;
        }

        private void Done_button_Click(object sender, EventArgs e)
        {
            // Close the socket and exit the application
            Close_Socket_and_Exit();
        }

        private void Connect_button_Click(object sender, EventArgs e)
        {   // Connect the Socket with a remote endpoint
            try
            {
                // Create the socket, for TCP use
                m_ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                m_ClientSocket.Blocking = true; // Socket operates in Blocking mode initially
            }
            catch // Handle any exceptions
            {
                Close_Socket_and_Exit();
            }
            try
            {   
                // Get the IP address from the appropriate text box
                String szIPAddress = IP_Address_textBox.Text;
                System.Net.IPAddress DestinationIPAddress = System.Net.IPAddress.Parse(szIPAddress);

                // Get the Port number from the appropriate text box
                String szPort = SendPort_textBox.Text;
                int iPort = System.Convert.ToInt16(szPort, 10);

                // Combine Address and Port to create an Endpoint
                m_remoteEndPoint = new System.Net.IPEndPoint(DestinationIPAddress, iPort);

                m_ClientSocket.Connect(m_remoteEndPoint);
                m_ClientSocket.Blocking = false;    // Socket is now switched to Non-Blocking mode for send/ receive activities
                Connect_button.Text = "Connected";
                Connect_button.Enabled = false;
                CloseConnection_button.Enabled = true;
                CloseConnection_button.Text = "Close Connection";
                m_CommunicationActivity_Timer.Start();  // Start the timer to perform periodic checking for received messages   
            }
            catch // Catch all exceptionss
            {   // If an exception occurs, display an error message
                Connect_button.Text = "(Connect attempt failed)\nRetry Connect";
            }
        }

        public void OnTimedEvent_PeriodicCommunicationActivityCheck(Object myObject, EventArgs myEventArgs)
        {   // Periodic check whether a message has been received    
            try
            {
                delay = new System.Timers.Timer();
                delay.Elapsed += timer_Tick; // Set event handler method for timer
                delay.Interval = 3000;              // Timer will tick 3 seconds
                delay.Enabled = false;
                delay.AutoReset = false;

                EndPoint RemoteEndPoint = (EndPoint)m_remoteEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int iReceiveByteCount;
                iReceiveByteCount = m_ClientSocket.ReceiveFrom(ReceiveBuffer, ref RemoteEndPoint);
                byte[] data = new byte[iReceiveByteCount];
                Array.Copy(ReceiveBuffer, data, iReceiveByteCount); //make sure byte[] is the same length as the recieved byte[]
                Message_PDU myPDU = DeSerialize(data); //convert the received byte[] back into a structure

                

                if (0 < iReceiveByteCount)
                {
                    if (myPDU.action.Equals("!!") && playernameBox.Text == myPDU.index)
                    {
                        chooseButton.Text = "You have been connected to the game!";
                        chooseButton.Enabled = false;
                        enemynameBox.Text = myPDU.playerChoice;
                        turnLabel.Text = enemynameBox.Text + " turn";
                    }
                    else if (myPDU.action.Equals("!!") && playernameBox.Text != myPDU.index)
                    {
                        opponentsList.Items.Remove(myPDU.index);
                        opponentsList.Items.Remove(myPDU.playerChoice);
                    }
                    else if (myPDU.action.Equals("--")) {
                        if (myPDU.index != playernameBox.Text)
                        {
                            opponentsList.BeginUpdate();

                            opponentsList.Items.Add(myPDU.index);

                            opponentsList.EndUpdate();
                            //opponentsList.Refresh();
                        }
                    }
                    else if (myPDU.action.Equals("**") && myPDU.index == playernameBox.Text)
                    {
                        winner_label.Text = myPDU.playerChoice;
                    }
                    else if (myPDU.action.Equals("rps") && myPDU.index == playernameBox.Text)
                    {
                        String oc = myPDU.playerChoice; // opponent choice
                        turnLabel.Text = "Your move!";
                        rock_button.Enabled = true;
                        paper_button.Enabled = true;
                        scissor_button.Enabled = true;
                        string winner = "";

                        if (myChoice != null && oc != null)
                        {
                            if (myChoice.Text == oc)
                            {
                                winner_label.Text = "It's a Tie, you both chose " + myChoice.Text + "!";
                                winner = "It's a Tie, you both chose "+ myChoice.Text +"!";
                            }
                            else if (myChoice.Text == "Paper" && oc == "Rock") {
                                winner_label.Text = "You won, your opponent chose Paper, Rock beats Paper!";
                                winner = "You lost, your opponent chose Rock, Rock beats Paper!";
                            }
                            else if (myChoice.Text == "Paper" && oc == "Scissors")
                            {
                                winner_label.Text = "You Lost, your opponent chose Scissors, Scissors beats Paper!";
                                winner = "You Won, your opponent chose Paper, Scissors beats Paper!";
                            }
                            else if (myChoice.Text == "Rock" && oc == "Paper")
                            {
                                winner_label.Text = "You lost, your opponent chose Rock, Rock beats Paper!";
                                winner = "You won, your opponent chose Paper, Rock beats Paper!";
                            }
                            else if (myChoice.Text == "Rock" && oc == "Scissors")
                            {
                                winner_label.Text = "You won, your opponent chose Scissors, Rock beats Scissors!";
                                winner = "You lost, your opponent chose Rock, Rock beats Scissors!";
                            }
                            else if (myChoice.Text == "Scissors" && oc == "Paper")
                            {
                                winner_label.Text = "You won, your opponent chose Paper, Scissors beats Paper!!";
                                winner = "You lost, your opponent chose Scissors, Scissors beats Paper!!";
                            }
                            else if (myChoice.Text == "Scissors" && oc == "Rock")
                            {
                                winner_label.Text = "You lost, your opponent chose Rock, Rock beats Scissors!";
                                winner = "You won, your opponent chose Scissors, Rock beats Scissors!";
                            }
                            
                        }

                        myPDU.action = "**";
                        myPDU.index = enemynameBox.Text;
                        myPDU.playerChoice = winner;
                        byte[] byData = Serialize(myPDU); //convert structure into a byte[]
                        m_ClientSocket.Send(byData, SocketFlags.None);  
                    }
                    if (winner_label.Text != "")
                    {

                        delay.Enabled = true;
                       
                    }
                }
                
            }
            catch // Silently handle any exceptions
            {
            }
            
        }


        //converts a byte[] to a Message_PDU structure
        public Message_PDU DeSerialize(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Message_PDU structure = (Message_PDU)Marshal.PtrToStructure(ip, typeof(Message_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        private void Close_Socket_and_Exit()
        {
            try
            {
                m_ClientSocket.Shutdown(SocketShutdown.Both);
            }
            catch // Silently handle any exceptions
            {
            }
            try
            {
                m_ClientSocket.Close();
            }
            catch // Silently handle any exceptions
            {
            }
            this.Close();
        }

        private void CloseConnection_button_Click(object sender, EventArgs e)
        {
            try
            {
                myPDU.action = "Disconnect";
                myPDU.iMessageNumber = 0;
                byte[] byData = Serialize(myPDU);
                m_ClientSocket.Send(byData, SocketFlags.None);
                m_ClientSocket.Shutdown(SocketShutdown.Both);
                m_ClientSocket.Close();
                CloseConnection_button.Enabled = false;
                CloseConnection_button.Text = "Connection Closed";
                Connect_button.Enabled = true;
                Connect_button.Text = "Connect";
                //ReceivedMessage_textBox.Text = "";
                //Send_button.Enabled = false;
            }
            catch // Silently handle any exceptions
            {
            }
        }

        private void rock_button_Click(object sender, EventArgs e)
        {
            myChoice.Text = "Rock";
            turnLabel.Text = enemynameBox.Text + " turn";

            rock_button.Enabled = false;
            paper_button.Enabled = false;
            scissor_button.Enabled = false;

            myPDU.action = "rps";
            myPDU.index = enemynameBox.Text;
            myPDU.playerChoice = "Rock";
            byte[] byData = Serialize(myPDU); //convert structure into a byte[]
            m_ClientSocket.Send(byData, SocketFlags.None);
        }

        private void paper_button_Click(object sender, EventArgs e)
        {
            myChoice.Text = "Paper";
            turnLabel.Text = enemynameBox.Text + " turn";

            rock_button.Enabled = false;
            paper_button.Enabled = false;
            scissor_button.Enabled = false;

            myPDU.action = "rps";
            myPDU.index = enemynameBox.Text;
            myPDU.playerChoice = "Paper";
            byte[] byData = Serialize(myPDU); //convert structure into a byte[]
            m_ClientSocket.Send(byData, SocketFlags.None);
        }

        private void scissor_button_Click(object sender, EventArgs e)
        {

            myChoice.Text = "Scissors";
            turnLabel.Text = enemynameBox.Text + " turn";

            rock_button.Enabled = false;
            paper_button.Enabled = false;
            scissor_button.Enabled = false;

            myPDU.action = "rps";
            myPDU.index = enemynameBox.Text;
            myPDU.playerChoice = "Scissors";
            byte[] byData = Serialize(myPDU); //convert structure into a byte[]
            m_ClientSocket.Send(byData, SocketFlags.None);
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            if (playernameBox.Text != "")
            {
                try
                {
                    registerButton.Enabled = false;
                    registerButton.Text = "Registered";
                    playernameBox.Enabled = false;

                    myPDU.action = "--";
                    myPDU.index = playernameBox.Text;
                    myPDU.playerChoice = "1";
                    byte[] byData = Serialize(myPDU); //convert structure into a byte[]
                    m_ClientSocket.Send(byData, SocketFlags.None);
                }
                catch// Silently handle any exceptions
                {

                }
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Invoke(new Action(() => winner_label.Text = null));
            Invoke(new Action(() => myChoice.Text = ""));
            Invoke(new Action(() => opponent_choice.Text = ""));
            //delay.Enabled = false;
            //delay.Stop();
        }

        private void chooseButton_Click(object sender, EventArgs e)
        {
            rock_button.Enabled = true;
            paper_button.Enabled = true;
            scissor_button.Enabled = true;

            int i = opponentsList.SelectedIndex;
            enemynameBox.Text = opponentsList.Items[i].ToString();
            enemynameBox.Enabled = false;
            chooseButton.Enabled = false;
            chooseButton.Text = "You are in a game!";
            turnLabel.Text = "Your move";

            try
            {
                myPDU.action = "!!";
                myPDU.index = opponentsList.Items[i].ToString();
                myPDU.playerChoice = playernameBox.Text;
                byte[] byData = Serialize(myPDU); //convert structure into a byte[]
                m_ClientSocket.Send(byData, SocketFlags.None);
            }
            catch// Silently handle any exceptions
            {

            }
        }

       
    }
}
