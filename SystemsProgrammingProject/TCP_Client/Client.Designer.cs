﻿/*	File			TCP_Client_Form.Designer.cs
	Purpose			C# TCP_Client Application for demonstration purposes (use with TCP_Server)
	Author			Richard Anthony	(R.J.Anthony@gre.ac.uk)
	Date			December 2011
	
	Special notes:
		This code has been specially prepared to demonstrate TCP usage in Client-Server applications
		Students following the "Systems Programming" course may use this
		code as a starting point for the development of their coursework.
*/

namespace PSRGame
{
    partial class Client
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IP_Address_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.About_button = new System.Windows.Forms.Button();
            this.Done_button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SendPort_textBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CloseConnection_button = new System.Windows.Forms.Button();
            this.Connect_button = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.turnLabel = new System.Windows.Forms.Label();
            this.myChoice = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.winner_label = new System.Windows.Forms.Label();
            this.opponent_choice = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.registerButton = new System.Windows.Forms.Button();
            this.playernameBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.opponentsList = new System.Windows.Forms.ListBox();
            this.chooseButton = new System.Windows.Forms.Button();
            this.enemynameBox = new System.Windows.Forms.TextBox();
            this.scissor_button = new System.Windows.Forms.Button();
            this.paper_button = new System.Windows.Forms.Button();
            this.rock_button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // IP_Address_textBox
            // 
            this.IP_Address_textBox.Location = new System.Drawing.Point(161, 24);
            this.IP_Address_textBox.Name = "IP_Address_textBox";
            this.IP_Address_textBox.Size = new System.Drawing.Size(107, 20);
            this.IP_Address_textBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP Address of Destination";
            // 
            // About_button
            // 
            this.About_button.Location = new System.Drawing.Point(240, 382);
            this.About_button.Name = "About_button";
            this.About_button.Size = new System.Drawing.Size(106, 37);
            this.About_button.TabIndex = 2;
            this.About_button.Text = "About";
            this.About_button.UseVisualStyleBackColor = true;
            this.About_button.Click += new System.EventHandler(this.About_button_Click);
            // 
            // Done_button
            // 
            this.Done_button.Location = new System.Drawing.Point(390, 382);
            this.Done_button.Name = "Done_button";
            this.Done_button.Size = new System.Drawing.Size(106, 37);
            this.Done_button.TabIndex = 3;
            this.Done_button.Text = "Done";
            this.Done_button.UseVisualStyleBackColor = true;
            this.Done_button.Click += new System.EventHandler(this.Done_button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Port for Connecting";
            // 
            // SendPort_textBox
            // 
            this.SendPort_textBox.Location = new System.Drawing.Point(180, 50);
            this.SendPort_textBox.Name = "SendPort_textBox";
            this.SendPort_textBox.Size = new System.Drawing.Size(69, 20);
            this.SendPort_textBox.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CloseConnection_button);
            this.groupBox1.Controls.Add(this.Connect_button);
            this.groupBox1.Controls.Add(this.SendPort_textBox);
            this.groupBox1.Controls.Add(this.IP_Address_textBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(442, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 129);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Communication";
            // 
            // CloseConnection_button
            // 
            this.CloseConnection_button.Location = new System.Drawing.Point(166, 85);
            this.CloseConnection_button.Name = "CloseConnection_button";
            this.CloseConnection_button.Size = new System.Drawing.Size(109, 37);
            this.CloseConnection_button.TabIndex = 10;
            this.CloseConnection_button.Text = "Close Connection";
            this.CloseConnection_button.UseVisualStyleBackColor = true;
            this.CloseConnection_button.Click += new System.EventHandler(this.CloseConnection_button_Click);
            // 
            // Connect_button
            // 
            this.Connect_button.Location = new System.Drawing.Point(19, 85);
            this.Connect_button.Name = "Connect_button";
            this.Connect_button.Size = new System.Drawing.Size(141, 37);
            this.Connect_button.TabIndex = 9;
            this.Connect_button.Text = "Connect";
            this.Connect_button.UseVisualStyleBackColor = true;
            this.Connect_button.Click += new System.EventHandler(this.Connect_button_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.turnLabel);
            this.groupBox2.Controls.Add(this.myChoice);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.winner_label);
            this.groupBox2.Controls.Add(this.opponent_choice);
            this.groupBox2.Controls.Add(this.scissor_button);
            this.groupBox2.Controls.Add(this.paper_button);
            this.groupBox2.Controls.Add(this.rock_button);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(424, 335);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RSPGame";
            // 
            // turnLabel
            // 
            this.turnLabel.AutoSize = true;
            this.turnLabel.Location = new System.Drawing.Point(187, 226);
            this.turnLabel.Name = "turnLabel";
            this.turnLabel.Size = new System.Drawing.Size(0, 13);
            this.turnLabel.TabIndex = 9;
            // 
            // myChoice
            // 
            this.myChoice.AutoSize = true;
            this.myChoice.Location = new System.Drawing.Point(82, 303);
            this.myChoice.Name = "myChoice";
            this.myChoice.Size = new System.Drawing.Size(0, 13);
            this.myChoice.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Your Choice";
            // 
            // winner_label
            // 
            this.winner_label.AutoSize = true;
            this.winner_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winner_label.Location = new System.Drawing.Point(11, 191);
            this.winner_label.Name = "winner_label";
            this.winner_label.Size = new System.Drawing.Size(0, 15);
            this.winner_label.TabIndex = 6;
            // 
            // opponent_choice
            // 
            this.opponent_choice.AutoSize = true;
            this.opponent_choice.Location = new System.Drawing.Point(134, 303);
            this.opponent_choice.Name = "opponent_choice";
            this.opponent_choice.Size = new System.Drawing.Size(0, 13);
            this.opponent_choice.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.registerButton);
            this.groupBox3.Controls.Add(this.playernameBox);
            this.groupBox3.Location = new System.Drawing.Point(442, 147);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(294, 59);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Player Name";
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(166, 16);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(75, 23);
            this.registerButton.TabIndex = 1;
            this.registerButton.Text = "Register";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // playernameBox
            // 
            this.playernameBox.Location = new System.Drawing.Point(19, 20);
            this.playernameBox.Name = "playernameBox";
            this.playernameBox.Size = new System.Drawing.Size(100, 20);
            this.playernameBox.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.opponentsList);
            this.groupBox4.Controls.Add(this.chooseButton);
            this.groupBox4.Controls.Add(this.enemynameBox);
            this.groupBox4.Location = new System.Drawing.Point(442, 212);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(294, 135);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Opponents List";
            // 
            // opponentsList
            // 
            this.opponentsList.FormattingEnabled = true;
            this.opponentsList.Location = new System.Drawing.Point(8, 21);
            this.opponentsList.Name = "opponentsList";
            this.opponentsList.Size = new System.Drawing.Size(120, 95);
            this.opponentsList.TabIndex = 3;
            // 
            // chooseButton
            // 
            this.chooseButton.Location = new System.Drawing.Point(161, 45);
            this.chooseButton.Name = "chooseButton";
            this.chooseButton.Size = new System.Drawing.Size(102, 23);
            this.chooseButton.TabIndex = 2;
            this.chooseButton.Text = "Choose opponent";
            this.chooseButton.UseVisualStyleBackColor = true;
            this.chooseButton.Click += new System.EventHandler(this.chooseButton_Click);
            // 
            // enemynameBox
            // 
            this.enemynameBox.Location = new System.Drawing.Point(161, 19);
            this.enemynameBox.Name = "enemynameBox";
            this.enemynameBox.Size = new System.Drawing.Size(100, 20);
            this.enemynameBox.TabIndex = 1;
            // 
            // scissor_button
            // 
            this.scissor_button.BackgroundImage = global::PSRGame.Properties.Resources.icons8_hand_scissors_filled_50;
            this.scissor_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.scissor_button.Enabled = false;
            this.scissor_button.Location = new System.Drawing.Point(289, 50);
            this.scissor_button.Name = "scissor_button";
            this.scissor_button.Size = new System.Drawing.Size(109, 99);
            this.scissor_button.TabIndex = 3;
            this.scissor_button.UseVisualStyleBackColor = true;
            this.scissor_button.Click += new System.EventHandler(this.scissor_button_Click);
            // 
            // paper_button
            // 
            this.paper_button.BackgroundImage = global::PSRGame.Properties.Resources.icons8_hand_50;
            this.paper_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.paper_button.Enabled = false;
            this.paper_button.Location = new System.Drawing.Point(153, 50);
            this.paper_button.Name = "paper_button";
            this.paper_button.Size = new System.Drawing.Size(108, 99);
            this.paper_button.TabIndex = 2;
            this.paper_button.UseVisualStyleBackColor = true;
            this.paper_button.Click += new System.EventHandler(this.paper_button_Click);
            // 
            // rock_button
            // 
            this.rock_button.BackgroundImage = global::PSRGame.Properties.Resources.icons8_hand_rock_64;
            this.rock_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.rock_button.Enabled = false;
            this.rock_button.Location = new System.Drawing.Point(14, 50);
            this.rock_button.Name = "rock_button";
            this.rock_button.Size = new System.Drawing.Size(114, 99);
            this.rock_button.TabIndex = 1;
            this.rock_button.UseVisualStyleBackColor = true;
            this.rock_button.Click += new System.EventHandler(this.rock_button_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 428);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Done_button);
            this.Controls.Add(this.About_button);
            this.Name = "Client";
            this.Text = "TCP Client    C#";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox IP_Address_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button About_button;
        private System.Windows.Forms.Button Done_button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SendPort_textBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Connect_button;
        private System.Windows.Forms.Button CloseConnection_button;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.TextBox playernameBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox enemynameBox;
        private System.Windows.Forms.Button chooseButton;
        private System.Windows.Forms.Label winner_label;
        private System.Windows.Forms.Label opponent_choice;
        private System.Windows.Forms.Button scissor_button;
        private System.Windows.Forms.Button paper_button;
        private System.Windows.Forms.Button rock_button;
        private System.Windows.Forms.Label myChoice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label turnLabel;
        private System.Windows.Forms.ListBox opponentsList;
    }
}

